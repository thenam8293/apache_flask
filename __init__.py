# -*- coding: utf-8 -*-
from __future__ import division
import os
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine, inspect, or_, update,cast
import json
import sys
sys.path.append("../FlaskApp")
import re
from os import environ
import datetime as dt
import hashlib
from functools import wraps
from flask_sqlalchemy  import SQLAlchemy
from flask import Flask, render_template, redirect, url_for, request, session, flash, jsonify
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, Sequence, VARCHAR,NVARCHAR, DateTime, update

basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__,static_url_path='/static')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = os.urandom(24)
app.permanent_session_lifetime = dt.timedelta(minutes=900)


######
class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'this-really-needs-to-be-changed'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite:///static/DB/sm_tool.db')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgres://mtsnjitzrsqjnc:c3f35c039064960e441d21055bd26e844243755af73d5ac5d463c2ccf4e3b64c@ec2-50-19-232-205.compute-1.amazonaws.com:5432/dc70qs45s1ture')
app.config.from_object(Config)
db = SQLAlchemy(app)

# ID AUTO
# USER
class ID_auto(db.Model):
    __tablename__ = "ID_auto"
    id__ = db.Column('id__', String, nullable = False, primary_key = True)
    def __init__(self, id__):
        self.id__ = id__
    def __repr__(self):
        return str([self.id__])


class ID_auto_roa_min(db.Model):
    __tablename__ = "ID_auto_roa_min"
    id__ = db.Column('id__', String, nullable = False, primary_key = True)
    def __init__(self, id__):
        self.id__ = id__
    def __repr__(self):
        return str([self.id__])


# THONG TIN QUY HOACH
class Quy_hoach(db.Model):
    __tablename__ = "thong_tin_quy_hoach"
    thanh_pho = db.Column('thanh_pho', String, nullable = False, primary_key = True)
    quan = db.Column('quan', String, nullable = False, primary_key = True)
    duong = db.Column('duong', String, nullable = False, primary_key = True)
    doan_duong = db.Column('doan_duong', String, nullable = False, primary_key = True)
    quy_hoach_hien_huu = db.Column('quy_hoach_hien_huu', String, nullable = False, primary_key = True)
    quy_hoach_moi = db.Column('quy_hoach_moi', String, nullable = False, primary_key = True)
    quy_hoach_thoat_lu = db.Column('quy_hoach_thoat_lu', String, nullable = False, primary_key = True)
    quy_hoach_cong_trinh = db.Column('quy_hoach_cong_trinh', String, nullable = False, primary_key = True)
    ghi_chu = db.Column('ghi_chu', String, nullable = False, primary_key = True)

    def __init__(self, thanh_pho, quan, duong, doan_duong, quy_hoach_hien_huu, quy_hoach_moi, quy_hoach_thoat_lu, quy_hoach_cong_trinh, ghi_chu):
        self.thanh_pho = thanh_pho
        self.quan = quan
        self.duong = duong
        self.doan_duong = doan_duong
        self.quy_hoach_hien_huu = quy_hoach_hien_huu
        self.quy_hoach_moi = quy_hoach_moi
        self.quy_hoach_thoat_lu = quy_hoach_thoat_lu
        self.quy_hoach_cong_trinh = quy_hoach_cong_trinh
        self.ghi_chu = ghi_chu

    def __repr__(self):
        return str([self.thanh_pho, self.quan, self.duong, self.doan_duong, self.quy_hoach_hien_huu, self.quy_hoach_moi, self.quy_hoach_thoat_lu, self.quy_hoach_cong_trinh, self.ghi_chu])


# USER
class User_SM(db.Model):
    __tablename__ = "sm_user"
    name = db.Column('name', String, nullable = False)
    cmnd = db.Column('cmnd', String, nullable = False)
    mail = db.Column('mail', String, nullable = False)
    sdt = db.Column('sdt', String, nullable = False)
    username = db.Column('username', String, primary_key = True, nullable = False)
    password = db.Column('password', String, nullable = False)
    passhash = db.Column('passhash', String, nullable = False)
    ngay_khoi_tao = db.Column('ngay_khoi_tao', String, nullable = False)
    phan_quyen = db.Column('phan_quyen', String, nullable = False)
    trang_thai = db.Column('trang_thai', String, nullable = False)
    ngay_doi_pass = db.Column('ngay_doi_pass', String, nullable = False)

    def __init__(self, name, cmnd, mail, sdt, username, password, passhash, ngay_khoi_tao, phan_quyen, trang_thai, ngay_doi_pass):
        self.name = name
        self.cmnd = cmnd
        self.mail = mail
        self.sdt = sdt
        self.username = username
        self.password = password
        self.passhash = passhash
        self.ngay_khoi_tao = ngay_khoi_tao
        self.phan_quyen = phan_quyen
        self.trang_thai = trang_thai
        self.ngay_doi_pass = ngay_doi_pass

    def __repr__(self):
        return str([self.name, self.cmnd, self.mail, self.sdt, self.username, self.password, self.passhash, self.ngay_khoi_tao, self.phan_quyen, self.trang_thai, self.ngay_doi_pass])


# LOAI NHA
class Loai_nha(db.Model):
    __tablename__ = "loai_nha_tho_cu"
    loai_nha = db.Column('loai_nha', String, nullable = False, primary_key = True)
    don_gia = db.Column('don_gia', String, nullable = False, primary_key = True)
    def __init__(self, loai_nha, don_gia):
        self.loai_nha = loai_nha
        self.don_gia = don_gia
    def __repr__(self):
        return str([self.loai_nha, self.don_gia])


# NAM SU DUNG
class Nam_su_dung(db.Model):
    __tablename__ = "thoi_gian_su_dung"
    thoi_gian = db.Column('thoi_gian', String, nullable = False, primary_key = True)
    ti_le = db.Column('ti_le', String, nullable = False, primary_key = True)
    def __init__(self, thoi_gian, ti_le):
        self.thoi_gian = thoi_gian
        self.ti_le = ti_le
    def __repr__(self):
        return str([self.thoi_gian, self.ti_le])


# EVENT LOG
class Event_log(db.Model):
    __tablename__ = "event_log"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key= True)
    username = db.Column('username', String, nullable = False)
    time_process = db.Column('time_process', String, nullable = False)
    phan_loai = db.Column('phan_loai', String, nullable = False)
    du_lieu_nhap = db.Column('du_lieu_nhap', String, nullable = False)
    ket_qua = db.Column('ket_qua', String, nullable = False)
    def __init__(self, id_ticket, username, time_process, phan_loai, du_lieu_nhap, ket_qua):
        self.id_ticket = id_ticket
        self.username = username
        self.time_process = time_process
        self.phan_loai = phan_loai
        self.du_lieu_nhap = du_lieu_nhap
        self.ket_qua = ket_qua

    def __repr__(self):
        return str([self.id_ticket, self.username, self.time_process, self.phan_loai, self.du_lieu_nhap, self.ket_qua])


# ID TICKET THO CU
class Id_ticket(db.Model):
    __tablename__ = "id_ticket"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key= True)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    vi_tri = db.Column('vi_tri', String, nullable = False)
    dien_tich = db.Column('dien_tich', String, nullable = False)
    mat_tien = db.Column('mat_tien', String, nullable = False)
    hinh_dang = db.Column('hinh_dang', String, nullable = False)
    do_rong_ngo = db.Column('do_rong_ngo', String, nullable = False)
    kcach_truc_chinh = db.Column('kcach_truc_chinh', String, nullable = False)
    yeu_to_loi_the = db.Column('yeu_to_loi_the', String, nullable = False)
    yeu_to_bat_loi = db.Column('yeu_to_bat_loi', String, nullable = False)
    gia_truoc = db.Column('gia_truoc', String, nullable = False)
    gia_sau = db.Column('gia_sau', String, nullable = False)
    
    loai_nha = db.Column('loai_nha', String, nullable = False, primary_key = True)
    thoi_gian_su_dung = db.Column('thoi_gian_su_dung', String, nullable = False, primary_key = True)
    don_gia_ctxd = db.Column('don_gia_ctxd', String, nullable = False, primary_key = True)
    dien_tich_san_xd = db.Column('dien_tich_san_xd', String, nullable = False, primary_key = True)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False)    

    def __init__(self, id_ticket, dia_chi, vi_tri, dien_tich, mat_tien, hinh_dang, do_rong_ngo, kcach_truc_chinh, yeu_to_loi_the, yeu_to_bat_loi, gia_truoc, gia_sau, loai_nha, thoi_gian_su_dung, don_gia_ctxd, dien_tich_san_xd,username, time):
        self.id_ticket = id_ticket        
        self.dia_chi = dia_chi
        self.vi_tri = vi_tri
        self.dien_tich = dien_tich
        self.mat_tien = mat_tien
        self.hinh_dang = hinh_dang
        self.do_rong_ngo = do_rong_ngo
        self.kcach_truc_chinh = kcach_truc_chinh
        self.yeu_to_loi_the = yeu_to_loi_the
        self.yeu_to_bat_loi = yeu_to_bat_loi
        self.gia_truoc = gia_truoc
        self.gia_sau = gia_sau
        self.loai_nha = loai_nha
        self.thoi_gian_su_dung = thoi_gian_su_dung
        self.don_gia_ctxd = don_gia_ctxd
        self.dien_tich_san_xd = dien_tich_san_xd
        self.username = username
        self.time = time

    def __repr__(self):
        return str([self.id_ticket, self.dia_chi, self.vi_tri, self.dien_tich, self.mat_tien, self.hinh_dang, self.do_rong_ngo, self.kcach_truc_chinh, self.yeu_to_loi_the, self.yeu_to_bat_loi, self.gia_truoc, self.gia_sau, self.loai_nha, self.thoi_gian_su_dung, self.don_gia_ctxd, self.dien_tich_san_xd, self.username, self.time])


# ID TICKET BIET THU
class Id_ticket_BT(db.Model):
    __tablename__ = "id_ticket_biet_thu"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    dien_tich_dat = db.Column('dien_tich_dat', String, nullable = False)
    dien_tich_san_xd = db.Column('dien_tich_san_xd', String, nullable = False)
    don_gia_dat = db.Column('don_gia_dat', String, nullable = False)
    don_gia_ctxd = db.Column('don_gia_ctxd', String, nullable = False)
    tong_gia_xay_tho = db.Column('tong_gia_xay_tho', String, nullable = False)
    tong_gia_hoan_thien = db.Column('tong_gia_hoan_thien', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)

    def __init__(self, id_ticket, ten_du_an, dien_tich_dat, dien_tich_san_xd, don_gia_dat, don_gia_ctxd, tong_gia_xay_tho, tong_gia_hoan_thien, username, time, dia_chi):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.dien_tich_dat = dien_tich_dat
        self.dien_tich_san_xd = dien_tich_san_xd
        self.don_gia_dat = don_gia_dat
        self.don_gia_ctxd = don_gia_ctxd
        self.tong_gia_xay_tho = tong_gia_xay_tho
        self.tong_gia_hoan_thien = tong_gia_hoan_thien
        self.username = username
        self.time = time
        self.dia_chi = dia_chi

    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.dien_tich_dat, self.dien_tich_san_xd, self.don_gia_dat, self.don_gia_ctxd, self.tong_gia_xay_tho, self.tong_gia_hoan_thien, self.username, self.time, self.dia_chi])


# ID TICKET NGHI DUONG
class Id_ticket_ND(db.Model):
    __tablename__ = "id_ticket_nghi_duong"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    dien_tich_dat = db.Column('dien_tich_dat', String, nullable = False)
    dien_tich_san_xd = db.Column('dien_tich_san_xd', String, nullable = False)
    don_gia_dat = db.Column('don_gia_dat', String, nullable = False)
    don_gia_ctxd = db.Column('don_gia_ctxd', String, nullable = False)
    tong_gia_xay_tho = db.Column('tong_gia_xay_tho', String, nullable = False)
    tong_gia_hoan_thien = db.Column('tong_gia_hoan_thien', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)

    def __init__(self, id_ticket, ten_du_an, dien_tich_dat, dien_tich_san_xd, don_gia_dat, don_gia_ctxd, tong_gia_xay_tho, tong_gia_hoan_thien, username, time, dia_chi):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.dien_tich_dat = dien_tich_dat
        self.dien_tich_san_xd = dien_tich_san_xd
        self.don_gia_dat = don_gia_dat
        self.don_gia_ctxd = don_gia_ctxd
        self.tong_gia_xay_tho = tong_gia_xay_tho
        self.tong_gia_hoan_thien = tong_gia_hoan_thien
        self.username = username
        self.time = time
        self.dia_chi = dia_chi

    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.dien_tich_dat, self.dien_tich_san_xd, self.don_gia_dat, self.don_gia_ctxd, self.tong_gia_xay_tho, self.tong_gia_hoan_thien, self.username, self.time, self.dia_chi])


# ID TICKET CHUNG CU
class Id_ticket_CC(db.Model):
    __tablename__ = "id_ticket_chung_cu"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    dien_tich = db.Column('dien_tich', String, nullable = False)
    loai_dien_tich = db.Column('loai_dien_tich', String, nullable = False)
    don_gia = db.Column('don_gia', String, nullable = False)
    tong_gia = db.Column('tong_gia', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)

    def __init__(self, id_ticket, ten_du_an, dien_tich, loai_dien_tich, don_gia, tong_gia, username, time, dia_chi):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.dien_tich = dien_tich
        self.loai_dien_tich = loai_dien_tich
        self.don_gia = don_gia
        self.tong_gia = tong_gia
        self.username = username
        self.time = time
        self.dia_chi = dia_chi


    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.dien_tich, self.loai_dien_tich, self.don_gia, self.tong_gia, self.username, self.time, self.dia_chi])


# ID TICKET QUY HOACH
class Id_ticket_quy_hoach(db.Model):
    __tablename__ = "id_ticket_quy_hoach"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    vi_tri = db.Column('vi_tri', String, nullable = False)
    ket_qua = db.Column('ket_qua', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False)
    def __init__(self, id_ticket, dia_chi, vi_tri, ket_qua, username, time):
        self.id_ticket = id_ticket
        self.dia_chi = dia_chi
        self.vi_tri = vi_tri
        self.ket_qua = ket_qua
        self.username = username
        self.time = time

    def __repr__(self):
        return str([self.id_ticket, self.dia_chi, self.vi_tri, self.ket_qua, self.username, self.time])


# ID TICKET ROA BIET THU
class Ticket_roa_bt(db.Model):
    __tablename__ = "id_ticket_roa_biet_thu"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    roa = db.Column('roa', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = False, primary_key = True)
    loi_nhuan = db.Column('loi_nhuan', String, nullable = False, primary_key = True)
    loai_nha = db.Column('loai_nha', String, nullable = False, primary_key = True)
    ten_duong = db.Column('ten_duong', String, nullable = False, primary_key = True)
    ten_tang = db.Column('ten_tang', String, nullable = False, primary_key = True)
    ma_can = db.Column('ma_can', String, nullable = False, primary_key = True)
    

    def __init__(self, id_ticket, ten_du_an, roa, dia_chi, username, time, tong_gia_ts, loi_nhuan, loai_nha,ten_duong, ten_tang, ma_can):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.roa = roa
        self.dia_chi = dia_chi
        self.username = username
        self.time = time
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.loai_nha = loai_nha
        self.ten_duong = ten_duong
        self.ten_tang = ten_tang
        self.ma_can = ma_can

    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.roa, self.dia_chi, self.username, self.time, self.tong_gia_ts, self.loi_nhuan, self.loai_nha, self.ten_duong, self.ten_tang, self.ma_can])


# ID TICKET ROA CHUNG CU
class Ticket_roa_cc(db.Model):
    __tablename__ = "id_ticket_roa_chung_cu"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    roa = db.Column('roa', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False)
    dien_tich = db.Column('dien_tich', String, nullable = False)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = False)
    loi_nhuan = db.Column('loi_nhuan', String, nullable = False)
    don_gia = db.Column('don_gia', String, nullable = False)
    idts = db.Column('idts', String, nullable = False)
    def __init__(self, id_ticket, ten_du_an, roa, dia_chi, username, time, dien_tich, tong_gia_ts, loi_nhuan, don_gia, idts):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.roa = roa
        self.dia_chi = dia_chi
        self.username = username
        self.time = time
        self.dien_tich = dien_tich
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.don_gia = don_gia
        self.idts = idts
    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.roa, self.dia_chi, self.username, self.time, self.dien_tich, self.tong_gia_ts, self.loi_nhuan, self.don_gia, self.idts])


# ID TICKET ROA THO CU
class Ticket_roa_tc(db.Model):
    __tablename__ = "id_ticket_roa_tho_cu"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    vi_tri = db.Column('vi_tri', String, nullable = False)
    roa = db.Column('roa', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False, primary_key = True)
    idts = db.Column('idts', String, nullable = False, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = False, primary_key = True)
    loi_nhuan = db.Column('loi_nhuan', String, nullable = False, primary_key = True)

    def __init__(self, id_ticket, dia_chi, vi_tri, roa, username, time, idts, tong_gia_ts, loi_nhuan):
        self.id_ticket = id_ticket
        self.dia_chi = dia_chi
        self.vi_tri = vi_tri
        self.roa = roa
        self.username = username
        self.time = time
        self.idts = idts
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
    def __repr__(self):
        return str([self.id_ticket, self.dia_chi, self.vi_tri, self.roa, self.username, self.time, self.idts, self.tong_gia_ts, self.loi_nhuan])


# ID TICKET ROA NGHI DUONG
class Ticket_roa_nd(db.Model):
    __tablename__ = "id_ticket_roa_nghi_duong"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    roa = db.Column('roa', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = False, primary_key = True)
    loi_nhuan = db.Column('loi_nhuan', String, nullable = False, primary_key = True)
    loai_nha = db.Column('loai_nha', String, nullable = False, primary_key = True)
    ten_duong = db.Column('ten_duong', String, nullable = False, primary_key = True)
    ten_tang = db.Column('ten_tang', String, nullable = False, primary_key = True)
    ma_can = db.Column('ma_can', String, nullable = False, primary_key = True)  

    def __init__(self, id_ticket, ten_du_an, roa, dia_chi, username, time, tong_gia_ts, loi_nhuan, loai_nha,ten_duong, ten_tang, ma_can):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.roa = roa
        self.dia_chi = dia_chi
        self.username = username
        self.time = time
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.loai_nha = loai_nha
        self.ten_duong = ten_duong
        self.ten_tang = ten_tang
        self.ma_can = ma_can

    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.roa, self.dia_chi, self.username, self.time, self.tong_gia_ts, self.loi_nhuan, self.loai_nha, self.ten_duong, self.ten_tang, self.ma_can])


# ID TICKET ROA TSDB DU AN
class Ticket_roa_min_du_an(db.Model):
    __tablename__ = "id_ticket_roa_min_du_an"
    id_ticket = db.Column('id_ticket', String, nullable = False, primary_key = True)
    ten_du_an = db.Column('ten_du_an', String, nullable = False)
    ten_duong = db.Column('ten_duong', String, nullable = False, primary_key = True)
    ten_tang = db.Column('ten_tang', String, nullable = False, primary_key = True)
    ma_can = db.Column('ma_can', String, nullable = False, primary_key = True)  
    roa = db.Column('roa', String, nullable = False)
    dia_chi = db.Column('dia_chi', String, nullable = False)
    username = db.Column('username', String, nullable = False)
    time = db.Column('time', String, nullable = False, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = False, primary_key = True)
    loi_nhuan = db.Column('loi_nhuan', String, nullable = False, primary_key = True)
    loai_nha = db.Column('loai_nha', String, nullable = False, primary_key = True)

    def __init__(self, id_ticket, ten_du_an, ten_duong, ten_tang, ma_can, roa, dia_chi, username, time, tong_gia_ts, loi_nhuan, loai_nha):
        self.id_ticket = id_ticket
        self.ten_du_an = ten_du_an
        self.ten_duong = ten_duong
        self.ten_tang = ten_tang
        self.ma_can = ma_can
        self.roa = roa
        self.dia_chi = dia_chi
        self.username = username
        self.time = time
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.loai_nha = loai_nha

    def __repr__(self):
        return str([self.id_ticket, self.ten_du_an, self.ten_duong, self.ten_tang, self.ma_can, self.roa, self.dia_chi, self.username, self.time, self.tong_gia_ts, self.loi_nhuan, self.loai_nha])



# ID TICKET ROA TSDB THO CU
class Ticket_roa_min_tc(db.Model):
    __tablename__ = "id_ticket_roa_min_tho_cu"
    id_ticket = db.Column('id_ticket', String, nullable = True, primary_key = True)
    dia_chi = db.Column('dia_chi', String, nullable = True)
    vi_tri = db.Column('vi_tri', String, nullable = True)
    roa = db.Column('roa', String, nullable = True)
    username = db.Column('username', String, nullable = True)
    time = db.Column('time', String, nullable = True, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = True, primary_key = True)
    loi_nhuan = db.Column('loi_nhuan', String, nullable = True, primary_key = True)
    loai_bds = db.Column('loai_bds', String, nullable = True, primary_key = True)
    dien_tich_dat = db.Column('dien_tich_dat', String, nullable = True, primary_key = True)
    dien_tich_san_xd = db.Column('dien_tich_san_xd', String, nullable = True, primary_key = True)
    thoi_gian_su_dung = db.Column('thoi_gian_su_dung', String, nullable = True, primary_key = True)

    def __init__(self, id_ticket, dia_chi, vi_tri, roa, username, time, tong_gia_ts, loi_nhuan, loai_bds, dien_tich_dat, dien_tich_san_xd, thoi_gian_su_dung):
        self.id_ticket = id_ticket
        self.dia_chi = dia_chi
        self.vi_tri = vi_tri
        self.roa = roa
        self.username = username
        self.time = time
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.loai_bds = loai_bds
        self.dien_tich_dat = dien_tich_dat
        self.dien_tich_san_xd = dien_tich_san_xd
        self.thoi_gian_su_dung = thoi_gian_su_dung
    def __repr__(self):
        return str([self.id_ticket, self.dia_chi, self.vi_tri, self.roa, self.username, self.time, self.tong_gia_ts, self.loi_nhuan,  self.loai_bds, self.dien_tich_dat, self.dien_tich_san_xd, self.thoi_gian_su_dung])


# KHUNG GIA UY BAN
class Khung_gia_uy_ban(db.Model):
    __tablename__ = "Khung_gia_uy_ban"
    thanh_pho = db.Column('thanh_pho', String, primary_key = True, nullable = False)
    quan_huyen = db.Column('quan_huyen', String, primary_key = True, nullable = False)
    tuyen_duong = db.Column('tuyen_duong', String, primary_key = True, nullable = False)
    doan_tu_den = db.Column('doan_tu_den', String, primary_key = True, nullable = False)
    VT1 = db.Column('VT1', String, primary_key = True, nullable = False)
    VT2 = db.Column('VT2', String, primary_key = True, nullable = False)
    VT3 = db.Column('VT3', String, primary_key = True, nullable = False)
    VT4 = db.Column('VT4', String, primary_key = True, nullable = False)
    VT5 = db.Column('VT5', String, primary_key = True, nullable = False)
    def __init__(self, thanh_pho,quan_huyen,tuyen_duong,doan_tu_den,VT1,VT2,VT3,VT4,VT5):
        self.thanh_pho
        self.quan_huyen
        self.tuyen_duong
        self.doan_tu_den
        self.VT1
        self.VT2
        self.VT3
        self.VT4
        self.VT5
    def __repr__(self):
        return str([self.thanh_pho, self.quan_huyen, self.tuyen_duong, self.doan_tu_den, self.VT1, self.VT2, self.VT3, self.VT4, self.VT5])


# DATA MB
class Data_MB(db.Model):
    __tablename__ = "Data_MB"
    Tinh_thanh = db.Column('Tinh_thanh', String, primary_key = True, nullable = False)
    Quan = db.Column('Quan', String, primary_key = True, nullable = False)
    Duong = db.Column('Duong', String, primary_key = True, nullable = False)
    Doan_duong = db.Column('Doan_duong', String, primary_key = True, nullable = False)
    Vi_tri = db.Column('Vi_tri', String, primary_key = True, nullable = False)
    Gia_UBND = db.Column('Gia_UBND', String, primary_key = True, nullable = False)
    Gia_thi_truong = db.Column('Gia_thi_truong', String, primary_key = True, nullable = False)
    Thong_tin_quy_hoach = db.Column('Thong_tin_quy_hoach', String, primary_key = True, nullable = False)
    Dia_chi = db.Column('Dia_chi', String, primary_key = True, nullable = False)
    Mien = db.Column('Mien', String, primary_key = True, nullable = False)

    def __init__(self, Tinh_thanh, Quan, Duong, Doan_duong, Vi_tri, Gia_UBND, Gia_thi_truong, Thong_tin_quy_hoach, Dia_chi, Mien):
        self.Tinh_thanh
        self.Quan
        self.Duong
        self.Doan_duong
        self.Vi_tri
        self.Gia_UBND
        self.Gia_thi_truong
        self.Thong_tin_quy_hoach
        self.Dia_chi
        self.Mien
    def __repr__(self):
        return str([self.Tinh_thanh, self.Quan, self.Duong, self.Doan_duong, self.Vi_tri, self.Gia_UBND, self.Gia_thi_truong, self.Thong_tin_quy_hoach, self.Dia_chi, self.Mien])


# DATA CHUNG CU
class Data_chung_cu(db.Model):
    __tablename__ = "data_chung_cu"
    ten_du_an = db.Column('ten_du_an', String, primary_key = True, nullable = False)
    ten_toa_duong_day_khu = db.Column('ten_toa_duong_day_khu', String, primary_key = True, nullable = False)
    ten_tang_loai_nha = db.Column('ten_tang_loai_nha', String, primary_key = True, nullable = False)
    ma_can = db.Column('ma_can', String, primary_key = True, nullable = False)
    dien_tich = db.Column('dien_tich', String, primary_key = True, nullable = False)
    loai_dien_tich = db.Column('loai_dien_tich', String, primary_key = True, nullable = False)
    don_gia = db.Column('don_gia', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)

    def __init__(self, ten_du_an, ten_toa_duong_day_khu, ten_tang_loai_nha, ma_can, dien_tich, loai_dien_tich, don_gia, dia_chi):
        self.ten_du_an
        self.ten_toa_duong_day_khu
        self.ten_tang_loai_nha
        self.ma_can
        self.dien_tich
        self.loai_dien_tich
        self.don_gia
        self.dia_chi
    def __repr__(self):
        return str([self.ten_du_an, self.ten_toa_duong_day_khu, self.ten_tang_loai_nha, self.ma_can, self.dien_tich, self.loai_dien_tich, self.don_gia, self.dia_chi])


# BDS BIET THU
class BDS_biet_thu(db.Model):
    __tablename__ = "bds_lien_ke_bt"
    ten_du_an = db.Column('ten_du_an', String, primary_key = True, nullable = False)
    ten_duong = db.Column('ten_duong', String, primary_key = True, nullable = False)
    ten_tang = db.Column('ten_tang', String, primary_key = True, nullable = False)
    ma_can = db.Column('ma_can', String, primary_key = True, nullable = False)
    dien_tich_dat = db.Column('dien_tich_dat', String, primary_key = True, nullable = False)
    dien_tich_san_xay_dung = db.Column('dien_tich_san_xay_dung', String, primary_key = True, nullable = False)
    tong_gia_tri_xay_tho = db.Column('tong_gia_tri_xay_tho', String, primary_key = True, nullable = False)
    tong_gia_tri_hoan_thien = db.Column('tong_gia_tri_hoan_thien', String, primary_key = True, nullable = False)
    don_gia_dat = db.Column('don_gia_dat', String, primary_key = True, nullable = False)
    don_gia_ctxd = db.Column('don_gia_ctxd', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)
    def __init__(self, ten_du_an, ten_duong, ten_tang, ma_can, dien_tich_dat, dien_tich_san_xay_dung, tong_gia_tri_xay_tho, tong_gia_tri_hoan_thien, don_gia_dat, don_gia_ctxd, dia_chi):
        self.ten_du_an
        self.ten_duong
        self.ten_tang
        self.ma_can
        self.dien_tich_dat
        self.dien_tich_san_xay_dung
        self.tong_gia_tri_xay_tho
        self.tong_gia_tri_hoan_thien
        self.don_gia_dat
        self.don_gia_ctxd
        self.dia_chi
    def __repr__(self):
        return str([self.ten_du_an, self.ten_duong, self.ten_tang, self.ma_can, self.dien_tich_dat, self.dien_tich_san_xay_dung, self.tong_gia_tri_xay_tho, self.tong_gia_tri_hoan_thien, self.don_gia_dat, self.don_gia_ctxd, self.dia_chi])


# BDS NGHI DUONG
class BDS_nghi_duong(db.Model):
    __tablename__ = "bds_nghi_duong"
    ten_du_an = db.Column('ten_du_an', String, primary_key = True, nullable = False)
    ten_duong = db.Column('ten_duong', String, primary_key = True, nullable = False)
    ten_tang = db.Column('ten_tang', String, primary_key = True, nullable = False)
    ma_can = db.Column('ma_can', String, primary_key = True, nullable = False)
    dien_tich_dat = db.Column('dien_tich_dat', String, primary_key = True, nullable = False)
    dien_tich_san_xay_dung = db.Column('dien_tich_san_xay_dung', String, primary_key = True, nullable = False)
    tong_gia_tri_xay_tho = db.Column('tong_gia_tri_xay_tho', String, primary_key = True, nullable = False)
    tong_gia_tri_hoan_thien = db.Column('tong_gia_tri_hoan_thien', String, primary_key = True, nullable = False)
    don_gia_dat = db.Column('don_gia_dat', String, primary_key = True, nullable = False)
    don_gia_ctxd = db.Column('don_gia_ctxd', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)
    def __init__(self, ten_du_an, ten_duong, ten_tang, ma_can, dien_tich_dat, dien_tich_san_xay_dung, tong_gia_tri_xay_tho, tong_gia_tri_hoan_thien, don_gia_dat, don_gia_ctxd, dia_chi):
        self.ten_du_an
        self.ten_duong
        self.ten_tang
        self.ma_can
        self.dien_tich_dat
        self.dien_tich_san_xay_dung
        self.tong_gia_tri_xay_tho
        self.tong_gia_tri_hoan_thien
        self.don_gia_dat
        self.don_gia_ctxd
        self.dia_chi
    def __repr__(self):
        return str([self.ten_du_an, self.ten_duong, self.ten_tang, self.ma_can, self.dien_tich_dat, self.dien_tich_san_xay_dung, self.tong_gia_tri_xay_tho, self.tong_gia_tri_hoan_thien, self.don_gia_dat, self.don_gia_ctxd, self.dia_chi])


# YEU TO
class Yeu_to(db.Model):
    __tablename__ = "yeu_to"
    yeu_to = db.Column('yeu_to', String, primary_key = True, nullable = False)
    ti_le = db.Column('ti_le', String, primary_key = True, nullable = False)
    mien = db.Column('mien', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    phan_loai = db.Column('phan_loai', String, primary_key = True, nullable = False)
    def __init__(self, yeu_to, ti_le, mien, vi_tri, phan_loai):
        self.yeu_to
        self.ti_le
        self.mien
        self.vi_tri
        self.phan_loai
    def __repr__(self):
        return str([self.yeu_to, self.ti_le, self.mien, self.vi_tri, self.phan_loai])


# MAT TIEN
class Mat_tien(db.Model):
    __tablename__ = "mat_tien"
    mat_tien = db.Column('mat_tien', String, primary_key = True, nullable = False)
    ti_le = db.Column('ti_le', String, primary_key = True, nullable = False)
    mien = db.Column('mien', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    def __init__(self, mat_tien, ti_le, mien, vi_tri):
        self.mat_tien
        self.ti_le
        self.mien
        self.vi_tri
    def __repr__(self):
        return str([self.mat_tien, self.ti_le, self.mien, self.vi_tri])


# QUY MO
class Quy_mo(db.Model):
    __tablename__ = "quy_mo"
    quy_mo = db.Column('quy_mo', String, primary_key = True, nullable = False)
    ti_le = db.Column('ti_le', String, primary_key = True, nullable = False)
    mien = db.Column('mien', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    def __init__(self, quy_mo, ti_le, mien, vi_tri):
        self.quy_mo
        self.ti_le
        self.mien
        self.vi_tri
    def __repr__(self):
        return str([self.quy_mo, self.ti_le, self.mien, self.vi_tri])


# HINH DANG
class Hinh_dang(db.Model):
    __tablename__ = "hinh_dang"
    hinh_dang = db.Column('hinh_dang', String, primary_key = True, nullable = False)
    ti_le = db.Column('ti_le', String, primary_key = True, nullable = False)
    mien = db.Column('mien', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    def __init__(self, hinh_dang, ti_le, mien, vi_tri):
        self.hinh_dang
        self.ti_le
        self.mien
        self.vi_tri
    def __repr__(self):
        return str([self.hinh_dang, self.ti_le, self.mien, self.vi_tri])


# DO RONG NGO
class Do_rong_ngo(db.Model):
    __tablename__ = "do_rong_ngo"
    Tinh_thanh = db.Column('Tinh_thanh', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    khoang_cach = db.Column('khoang_cach', String, primary_key = True, nullable = False)
    ti_le = db.Column('ti_le', String, primary_key = True, nullable = False)
    mien = db.Column('mien', String, primary_key = True, nullable = False)
    
    def __init__(self, Tinh_thanh, vi_tri, khoang_cach, ti_le, mien):
        self.Tinh_thanh
        self.vi_tri
        self.khoang_cach        
        self.ti_le
        self.mien
    def __repr__(self):
        return str([self.Tinh_thanh, self.vi_tri, self.khoang_cach , self.ti_le, self.mien])


# KHOANG CACH TRUC CHINH
class Khoang_cach_truc(db.Model):
    __tablename__ = "khoang_cach_den_truc_chinh"
    Tinh_thanh = db.Column('Tinh_thanh', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    khoang_cach = db.Column('khoang_cach', String, primary_key = True, nullable = False)
    ti_le = db.Column('ti_le', String, primary_key = True, nullable = False)
    mien = db.Column('mien', String, primary_key = True, nullable = False)
    
    def __init__(self, Tinh_thanh, vi_tri, khoang_cach, ti_le, mien):
        self.Tinh_thanh
        self.vi_tri
        self.khoang_cach        
        self.ti_le
        self.mien
    def __repr__(self):
        return str([self.Tinh_thanh, self.vi_tri, self.khoang_cach , self.ti_le, self.mien])


#DAC DIEM VI TRI
class Dac_diem_VT(db.Model):
    __tablename__ = "dac_diem_vi_tri"
    thanh_pho = db.Column('thanh_pho', String, primary_key = True, nullable = False)
    vi_tri = db.Column('vi_tri', String, primary_key = True, nullable = False)
    dac_diem = db.Column('dac_diem', String, primary_key = True, nullable = False)
    def __init__(self, thanh_pho, vi_tri, dac_diem):
        self.thanh_pho
        self.vi_tri
        self.dac_diem
    def __repr__(self):
        return str([self.thanh_pho, self.vi_tri, self.dac_diem])


# ROA
# ROA THO CU
class ROA_tho_cu(db.Model):
    __tablename__ = "roa_tho_cu"
    Tinh_thanh = db.Column('Tinh_thanh', String, primary_key = True, nullable = False)
    Quan = db.Column('Quan', String, primary_key = True, nullable = False)
    Duong = db.Column('Duong', String, primary_key = True, nullable = False)
    Doan_duong = db.Column('Doan_duong', String, primary_key = True)
    Vi_tri = db.Column('Vi_tri', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)
    roa1 = db.Column('roa1', String, primary_key = True)
    roa2 = db.Column('roa2', String, primary_key = True)
    roa3 = db.Column('roa3', String, primary_key = True)
    roa4 = db.Column('roa4', String, primary_key = True)
    roa5 = db.Column('roa5', String, primary_key = True)
    roa6 = db.Column('roa6', String, primary_key = True)
    roa7 = db.Column('roa7', String, primary_key = True)
    roa8 = db.Column('roa8', String, primary_key = True)
    def __init__(self, Tinh_thanh, Quan, Duong, Doan_duong, Vi_tri, dia_chi, roa1, roa2, roa3, roa4, roa5, roa6, roa7, roa8):
        self.Tinh_thanh = Tinh_thanh
        self.Quan = Quan
        self.Duong = Duong
        self.Doan_duong = Doan_duong
        self.Vi_tri = Vi_tri
        self.dia_chi = dia_chi
        self.roa1 = roa1
        self.roa2 = roa2
        self.roa3 = roa3
        self.roa4 = roa4
        self.roa5 = roa5
        self.roa6 = roa6
        self.roa7 = roa7
        self.roa8 = roa8
    def __repr__(self):
        return str([self.Tinh_thanh, self.Quan, self.Duong, self.Doan_duong, self.Vi_tri, self.dia_chi, self.roa1, self.roa2, self.roa3, self.roa4, self.roa5, self.roa6, self.roa7, self.roa8])


# ROA CHUNG CU
class ROA_chung_cu(db.Model):
    __tablename__ = "roa_chung_cu"
    ten_du_an = db.Column('ten_du_an', String, primary_key = True, nullable = False)
    ten_toa_duong_day_khu = db.Column('ten_toa_duong_day_khu', String, primary_key = True, nullable = False)
    ten_tang_loai_nha = db.Column('ten_tang_loai_nha', String, primary_key = True, nullable = False)
    ma_can = db.Column('ma_can', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)
    roa1 = db.Column('roa1', String, primary_key = True)
    dien_tich = db.Column('dien_tich', String, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, primary_key = True)
    loi_nhuan = db.Column('loi_nhuan', String, primary_key = True)
    don_gia = db.Column('don_gia', String, primary_key = True)
    roa6 = db.Column('roa6', String, primary_key = True)
    roa7 = db.Column('roa7', String, primary_key = True)
    roa8 = db.Column('roa8', String, primary_key = True)
    def __init__(self, ten_du_an, ten_toa_duong_day_khu, ten_tang_loai_nha, ma_can, dia_chi, roa1, dien_tich, tong_gia_ts, loi_nhuan, don_gia, roa6, roa7, roa8):
        self.ten_du_an = ten_du_an
        self.ten_toa_duong_day_khu = ten_toa_duong_day_khu
        self.ten_tang_loai_nha = ten_tang_loai_nha
        self.ma_can = ma_can
        self.dia_chi = dia_chi
        self.roa1 = roa1
        self.dien_tich = dien_tich
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.don_gia = don_gia
        self.roa6 = roa6
        self.roa7 = roa7
        self.roa8 = roa8
    def __repr__(self):
        return str([self.ten_du_an, self.ten_toa_duong_day_khu, self.ten_tang_loai_nha, self.ma_can, self.dia_chi, self.roa1, self.dien_tich, self.tong_gia_ts, self.loi_nhuan, self.don_gia, self.roa6, self.roa7, self.roa8])


# ROA BIET THU
class ROA_biet_thu(db.Model):
    __tablename__ = "roa_biet_thu"
    ten_du_an = db.Column('ten_du_an', String, primary_key = True, nullable = False)
    ten_duong = db.Column('ten_duong', String, primary_key = True, nullable = False)
    ten_tang = db.Column('ten_tang', String, primary_key = True, nullable = False)
    ma_can = db.Column('ma_can', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)
    roa1 = db.Column('roa1', String, primary_key = True, nullable = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, primary_key = True, nullable = True)
    loi_nhuan = db.Column('loi_nhuan', String, primary_key = True, nullable = True)
    loai_bds = db.Column('loai_bds', String, primary_key = True, nullable = True)
    roa5 = db.Column('roa5', String, primary_key = True, nullable = True)
    roa6 = db.Column('roa6', String, primary_key = True, nullable = True)
    roa7 = db.Column('roa7', String, primary_key = True, nullable = True)
    roa8 = db.Column('roa8', String, primary_key = True, nullable = True)
    def __init__(self, ten_du_an, ten_duong, ten_tang, ma_can, dia_chi, roa1, tong_gia_ts, loi_nhuan, loai_bds, roa5, roa6, roa7, roa8):
        self.ten_du_an = ten_du_an
        self.ten_duong = ten_duong
        self.ten_tang = ten_tang
        self.ma_can = ma_can
        self.dia_chi = dia_chi
        self.roa1 = roa1
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.loai_bds = loai_bds
        self.roa5 = roa5
        self.roa6 = roa6
        self.roa7 = roa7
        self.roa8 = roa8
    def __repr__(self):
        return str([self.ten_du_an, self.ten_duong, self.ten_tang, self.ma_can, self.dia_chi, self.roa1, self.tong_gia_ts, self.loi_nhuan, self.loai_bds, self.roa5, self.roa6, self.roa7, self.roa8])


# ROA NGHI DUONG
class ROA_nghi_duong(db.Model):
    __tablename__ = "roa_nghi_duong"
    ten_du_an = db.Column('ten_du_an', String, primary_key = True, nullable = False)
    ten_duong = db.Column('ten_duong', String, primary_key = True, nullable = False)
    ten_tang = db.Column('ten_tang', String, primary_key = True, nullable = False)
    ma_can = db.Column('ma_can', String, primary_key = True, nullable = False)
    dia_chi = db.Column('dia_chi', String, primary_key = True, nullable = False)
    roa1 = db.Column('roa1', String, primary_key = True, nullable = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, primary_key = True, nullable = True)
    loi_nhuan = db.Column('loi_nhuan', String, primary_key = True, nullable = True)
    loai_bds = db.Column('loai_bds', String, primary_key = True, nullable = True)
    roa5 = db.Column('roa5', String, primary_key = True, nullable = True)
    roa6 = db.Column('roa6', String, primary_key = True, nullable = True)
    roa7 = db.Column('roa7', String, primary_key = True, nullable = True)
    roa8 = db.Column('roa8', String, primary_key = True, nullable = True)
    def __init__(self, ten_du_an, ten_duong, ten_tang, ma_can, dia_chi, roa1, tong_gia_ts, loi_nhuan, loai_bds, roa5, roa6, roa7, roa8):
        self.ten_du_an = ten_du_an
        self.ten_duong = ten_duong
        self.ten_tang = ten_tang
        self.ma_can = ma_can
        self.dia_chi = dia_chi
        self.roa1 = roa1
        self.tong_gia_ts = tong_gia_ts
        self.loi_nhuan = loi_nhuan
        self.loai_bds = loai_bds
        self.roa5 = roa5
        self.roa6 = roa6
        self.roa7 = roa7
        self.roa8 = roa8
    def __repr__(self):
        return str([self.ten_du_an, self.ten_duong, self.ten_tang, self.ma_can, self.dia_chi, self.roa1, self.tong_gia_ts, self.loi_nhuan, self.loai_bds, self.roa5, self.roa6, self.roa7, self.roa8])


# ROA MIN DU AN
class ROA_min_du_an(db.Model):
    __tablename__ = "roa_min_du_an"
    ten_du_an = db.Column('ten_du_an', String, nullable = True, primary_key = True)
    ten_toa_duong_day_khu = db.Column('ten_toa_duong_day_khu', String, nullable = True, primary_key = True)
    ten_tang_loai_nha = db.Column('ten_tang_loai_nha', String, nullable = True, primary_key = True)
    ma_can = db.Column('ma_can', String, nullable = True, primary_key = True)
    dia_chi = db.Column('dia_chi', String, nullable = True, primary_key = True)
    roa = db.Column('roa', String, nullable = True, primary_key = True)
    loai_bds = db.Column('loai_bds', String, nullable = True, primary_key = True)
    tong_gia_ts = db.Column('tong_gia_ts', String, nullable = True, primary_key = True)
    def __init__(self, ten_du_an, ten_toa_duong_day_khu, ten_tang_loai_nha, ma_can, dia_chi, ROA, loai_bds, tong_gia_ts):
        self.ten_du_an = ten_du_an
        self.ten_toa_duong_day_khu = ten_toa_duong_day_khu
        self.ten_tang_loai_nha = ten_tang_loai_nha
        self.ma_can = ma_can
        self.dia_chi = dia_chi
        self.ROA = ROA
        self.loai_bds = loai_bds
        self.tong_gia_ts = tong_gia_ts
    def __repr__(self):
        return str([self.ten_du_an, self.ten_toa_duong_day_khu, self.ten_tang_loai_nha, self.ma_can, self.dia_chi, self.ROA, self.loai_bds, self.tong_gia_ts])


# ROA MIN THO CU
class ROA_min_tho_cu(db.Model):
    __tablename__ = "roa_min_tho_cu"
    Tinh_thanh = db.Column('Tinh_thanh', String, nullable = True, primary_key = True)
    Quan = db.Column('Quan', String, nullable = True, primary_key = True)
    Duong = db.Column('Duong', String, nullable = True, primary_key = True)
    Doan_duong = db.Column('Doan_duong', String, nullable = True, primary_key = True)
    Vi_tri = db.Column('Vi_tri', String, nullable = True, primary_key = True)
    roa = db.Column('roa', String, nullable = True, primary_key = True)
    gia_thi_truong = db.Column('gia_thi_truong', String, nullable = True, primary_key = True)
    def __init__(self, Tinh_thanh, Quan, Duong, Doan_duong, Vi_tri, roa, gia_thi_truong):
        self.Tinh_thanh = Tinh_thanh
        self.Quan = Quan
        self.Duong = Duong
        self.Doan_duong = Doan_duong
        self.Vi_tri = Vi_tri
        self.roa = roa
        self.gia_thi_truong = gia_thi_truong
    def __repr__(self):
        return str([self.Tinh_thanh, self.Quan, self.Duong, self.Doan_duong, self.Vi_tri, self.roa, self.gia_thi_truong])


def hash_user(user):
    return hashlib.md5(user.encode('utf-8')).hexdigest()

def dt_to_str(x):
    return '{} {}/{}/{}'.format(str(x)[11:16],str(x)[8:10],str(x)[5:7],str(x)[:4])

def str_to_dt(x):
    try:
        return dt.datetime.strptime(x,'%Y-%m-%d %H:%M:%S')
    except:
        return dt.datetime.strptime(x,'%Y-%m-%d')

def round_int(x):
    return int(round(x))

def convert_int(x):
    if x:
        return int(x.replace(',',''))
    else:
        return 0

def change_list_order(x,i):
    if i == 3:
        x = [x[1], x[2], x[0]]
    elif i == 4:
        x = [x[2], x[1], x[3], x[0]]
    return x

def add_comma(x):
    if len(x) <= 3:
        result = x
    else:
        no_part = len(x)//3
        result = ''
        if no_part == 1:
            part_1 = x[-3:]
            result = part_1
            result = x[:-3] + ',' + result
        else:
            part_1 = x[-3:]
            result = part_1
            for r in xrange(1, no_part):
                part_n = x[- 3*(r + 1): - 3*r]
                result = part_n + ',' + result
            if len(x)%3 == 0:
                pass
            else:
                result = x[: - 3*(r + 1)] + ',' + result
    return result

def bo_dau_tieng_viet(x):
    dict_co_dau = {'a': [u'á', u'à', u'ả', u'ã', u'ạ', u'ă', u'ắ', u'ằ', u'ẳ', u'ẵ', u'ặ', u'â', u'ấ', u'ầ', u'ẩ', u'ẫ', u'ậ'], 'e': [u'é', u'è', u'ẻ', u'ẽ', u'ẹ', u'ê', u'ế', u'ề', u'ể', u'ễ', u'ệ'], 'i': [u'í', u'ì', u'ỉ', u'ĩ', u'ị'], 'o': [u'ó', u'ò', u'ỏ', u'õ', u'ọ', u'ô', u'ố', u'ồ', u'ổ', u'ỗ', u'ộ', u'ơ', u'ớ', u'ờ', u'ở', u'ỡ', u'ợ'], 'u': [u'ú', u'ù', u'ủ', u'ũ', u'ụ', u'ư', u'ứ', u'ừ', u'ử', u'ữ', u'ự'], 'y': [u'ý', u'ỳ', u'ỷ', u'ỹ', u'ỵ'], 'd': [u'đ']}

    for r in dict_co_dau.keys():
        for r1 in dict_co_dau[r]:
            x = x.replace(r1, r)
    return x

def find_ko_dau(x, y):
    list_co_dau = [u'á', u'à', u'ả', u'ã', u'ạ', u'ắ', u'ằ', u'ẳ', u'ẵ', u'ặ', u'ấ', u'ầ', u'ẩ', u'ẫ', u'ậ', u'é', u'è', u'ẻ', u'ẽ', u'ẹ', u'ế', u'ề', u'ể', u'ễ', u'ệ', u'í', u'ì', u'ỉ', u'ĩ', u'ị', u'ó', u'ò', u'ỏ', u'õ', u'ọ', u'ố', u'ồ', u'ổ', u'ỗ', u'ộ', u'ớ', u'ờ', u'ở', u'ỡ', u'ợ', u'ú', u'ù', u'ủ', u'ũ', u'ụ', u'ứ', u'ừ', u'ử', u'ữ', u'ự', u'ý', u'ỳ', u'ỷ', u'ỹ', u'ỵ', u'ă', u'â', u'ê', u'ô', u'ơ', u'ư', u'đ']
    if True in [r in list_co_dau for r in y.lower()]:
        result = x.find(y)
    else:
        result = bo_dau_tieng_viet(x).find(bo_dau_tieng_viet(y))
    return result



def changepass_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if session['changepass'] == True:   
            return f(*args, **kwargs)            
        else:
            return redirect(url_for('changepass'))
    return wrap


def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if session['role'] == 'Admin':   
            return f(*args, **kwargs)
        else:
            return redirect(url_for('check_gia'))
    return wrap


#############

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:   
            return f(*args, **kwargs)            
        else:
            return redirect(url_for('login'))
    return wrap


def sorted_nicely(list_input):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(list_input, key = alphanum_key)


def ID_auto_create():
    id_old = db.session.query(ID_auto).all()[0].id__
    id_sub = str(int(id_old)+1)
    id_new = '0'*(8-len(id_sub)) + id_sub
    db.session.query(ID_auto).update({ID_auto.id__ : id_new})
    db.session.commit()
    return id_new


def ID_auto_create_roa_min():
    id_old = db.session.query(ID_auto_roa_min).all()[0].id__
    id_sub = str(int(id_old)+1)
    id_new = '0'*(8-len(id_sub)) + id_sub
    db.session.query(ID_auto_roa_min).update({ID_auto_roa_min.id__ : id_new})
    db.session.commit()
    return id_new


@app.route('/ajax_render_id_ticket',methods=['GET', 'POST'])
def ajax_render_id_ticket():
    Id_ticket = [r[0] for r in db.session.query(Event_log.id_ticket).order_by(Event_log.id_ticket.desc()).all()]
    return jsonify({'result':Id_ticket})


# -------------- ROA BIET THU ---------------
@app.route('/ajax_result_roa_biet_thu',methods=['GET', 'POST'])
@login_required
def ajax_result_roa_biet_thu():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    ten_tang = request.args['ten_tang']
    ma_can = request.args['ma_can']
    idts_bt = request.args['idts_bt']

    result = db.session.query(ROA_biet_thu.tong_gia_ts, ROA_biet_thu.roa1, ROA_biet_thu.loi_nhuan, ROA_biet_thu.dia_chi).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang, ma_can = ma_can).distinct().all()[0]
    tong_gia_ts = result.tong_gia_ts
    roa = result.roa1
    loi_nhuan = result.loi_nhuan
    dia_chi = result.dia_chi
    new_id = ID_auto_create()
    ngay_khoi_tao = dt.datetime.now()

    dia_chi_nhap = ma_can + "||" + ten_tang + "||" + ten_duong + "||" + ten_du_an
    dia_chi_roa_biet_thu_log = "|>TT17|" + str(dia_chi_nhap) + "*|*"
    roa_biet_thu_log = "|>KQ23|" + str(roa) + "*|*"
    tong_gia_ts_roa_biet_thu_log = "|>KQ24|" + str(tong_gia_ts) + "*|*"
    loi_nhuan_roa_biet_thu_log = "|>KQ25|" + str(loi_nhuan) + "*|*"

    du_lieu_nhap = dia_chi_roa_biet_thu_log
    ket_qua = roa_biet_thu_log + tong_gia_ts_roa_biet_thu_log + loi_nhuan_roa_biet_thu_log

    new_ticket = Ticket_roa_bt(
                                new_id,
                                ten_du_an,
                                roa,
                                dia_chi,
                                session['username'],
                                ngay_khoi_tao,
                                tong_gia_ts,
                                loi_nhuan,
                                'BĐS Biệt thự',
                                ten_duong,
                                ten_tang,
                                ma_can
                                )
    db.session.add(new_ticket)
    db.session.commit()

    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'roa_bds_biet_thu', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
                    'tong_gia_ts' : tong_gia_ts,
                    'roa' : roa,
                    'loi_nhuan' : loi_nhuan,
                    'dia_chi' : dia_chi,
                    'ten_du_an' : ten_du_an,
                    'ten_duong' : ten_duong,
                    'ten_tang' : ten_tang,
                    'ma_can' : ma_can,
                    'new_id' : new_id,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'idts_bt' : idts_bt
                    })


# -------------- ROA NGHI DUONG --------------
@app.route('/ajax_result_roa_nghi_duong',methods=['GET', 'POST'])
@login_required
def ajax_result_roa_nghi_duong():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    ten_tang = request.args['ten_tang']
    ma_can = request.args['ma_can']
    idts_nd = request.args['idts_nd']

    result = db.session.query(ROA_nghi_duong.tong_gia_ts, ROA_nghi_duong.roa1, ROA_nghi_duong.loi_nhuan, ROA_nghi_duong.dia_chi).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang, ma_can = ma_can).distinct().all()[0]
    tong_gia_ts = result.tong_gia_ts
    roa = result.roa1
    loi_nhuan = result.loi_nhuan
    dia_chi = result.dia_chi
    new_id = ID_auto_create()
    ngay_khoi_tao = dt.datetime.now()

    dia_chi_nhap = ma_can + "||" + ten_tang + "||" + ten_duong + "||" + ten_du_an
    dia_chi_roa_nghi_duong_log = "|>TT16|" + str(dia_chi_nhap) + "*|*"
    roa_nghi_duong_log = "|>KQ20|" + str(roa) + "*|*"
    tong_gia_ts_roa_nghi_duong_log = "|>KQ21|" + str(tong_gia_ts) + "*|*"
    loi_nhuan_roa_nghi_duong_log = "|>KQ22|" + str(loi_nhuan) + "*|*"

    du_lieu_nhap = dia_chi_roa_nghi_duong_log
    ket_qua = roa_nghi_duong_log + tong_gia_ts_roa_nghi_duong_log + loi_nhuan_roa_nghi_duong_log

    new_ticket = Ticket_roa_nd(
                                new_id,
                                ten_du_an,
                                roa,
                                dia_chi,
                                session['username'],
                                ngay_khoi_tao,
                                tong_gia_ts,
                                loi_nhuan,
                                'BĐS Nghỉ dưỡng',
                                ten_duong,
                                ten_tang,
                                ma_can
                                )
    db.session.add(new_ticket)
    db.session.commit()

    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'roa_bds_nghi_duong', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
                    'tong_gia_ts' : tong_gia_ts,
                    'roa' : roa,
                    'loi_nhuan' : loi_nhuan,
                    'dia_chi' : dia_chi,
                    'ten_du_an' : ten_du_an,
                    'ten_duong' : ten_duong,
                    'ten_tang' : ten_tang,
                    'ma_can' : ma_can,
                    'new_id' : new_id,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'idts_nd' : idts_nd
                    })


# --------------------- ROA CHUNG CU -----------------
@app.route('/ajax_result_roa_chung_cu', methods=['GET', 'POST'])
@login_required
def ajax_result_roa_chung_cu():
    ten_du_an = request.args['ten_du_an']
    toa_nha = request.args['toa_nha']
    ten_tang = request.args['ten_tang']
    ma_can = request.args['ma_can']
    dien_tich_raw = request.args['dien_tich']
    idts = request.args['idts_cc']
    if ma_can != '':
        result = db.session.query(ROA_chung_cu.dia_chi, ROA_chung_cu.roa1, ROA_chung_cu.dien_tich, ROA_chung_cu.don_gia).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = toa_nha, ten_tang_loai_nha = ten_tang, ma_can = ma_can).all()[0]
        dien_tich = result.dien_tich
    else:
        result = db.session.query(ROA_chung_cu.dia_chi, ROA_chung_cu.roa1, ROA_chung_cu.don_gia).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = toa_nha, ten_tang_loai_nha = ten_tang).all()[0]
        dien_tich = dien_tich_raw
    don_gia = result.don_gia
    dia_chi = result.dia_chi
    new_id = ID_auto_create()
    roa = result.roa1
    tong_gia_ts = float(don_gia) * float(dien_tich)

    loi_nhuan = tong_gia_ts * float(roa)

    ngay_khoi_tao = dt.datetime.now()
    dia_chi_roa_chung_cu_log = "|>TT18|" + str(dia_chi) + "*|*"
    idts_roa_chung_cu_log = "|>TT19|" + str(idts) + "*|*"
    ten_du_an_roa_chung_cu_log = "|>TT20|" + ma_can + "||" + ten_tang + "||" + toa_nha + "||" + ten_du_an + "*|*"
    dien_tich_roa_chung_cu_log = "|>TT21|" + str(dien_tich) + "*|*"

    roa_chung_cu_log = "|>KQ26|" + str(roa) + "*|*"
    tong_gia_ts_roa_chung_cu_log = "|>KQ27|" + str(tong_gia_ts) + "*|*"
    loi_nhuan_roa_chung_cu_log = "|>KQ28|" + str(loi_nhuan) + "*|*"

    du_lieu_nhap = dia_chi_roa_chung_cu_log + idts_roa_chung_cu_log + ten_du_an_roa_chung_cu_log + dien_tich_roa_chung_cu_log
    ket_qua = roa_chung_cu_log + tong_gia_ts_roa_chung_cu_log + loi_nhuan_roa_chung_cu_log
    # NEW TICKET
    new_ticket = Ticket_roa_cc(
                                new_id,
                                ma_can + "||" + ten_tang + "||" + toa_nha + "||" + ten_du_an,
                                roa,
                                dia_chi,
                                session['username'],
                                ngay_khoi_tao,
                                dien_tich,
                                tong_gia_ts,
                                loi_nhuan,
                                don_gia,
                                idts
                                )
    db.session.add(new_ticket)
    db.session.commit()
    # EVENT LOG
    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'roa_bds_chung_cu', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'ten_du_an' : ten_du_an,
                    'toa_nha' : toa_nha,
                    'ten_tang' : ten_tang,
                    'ma_can' : ma_can,
                    'dien_tich' : dien_tich,
                    'dia_chi' : dia_chi,
                    'don_gia' : don_gia,
                    'tong_gia_ts' : tong_gia_ts,
                    'roa' : roa,
                    'new_id' : new_id,
                    'loi_nhuan' : loi_nhuan,
                    })


# --------------------- IDTS CC ----------------------
@app.route('/ajax_idts_chung_cu_info', methods=['GET', 'POST'])
def ajax_idts_chung_cu_info():
    idts = request.args['idts_cc']
    result = db.session.query(Id_ticket_CC.ten_du_an, Id_ticket_CC.dien_tich).filter_by(id_ticket = idts).distinct().all()[0]
    ten_du_an = result.ten_du_an.split('||')[3]
    toa_nha = result.ten_du_an.split('||')[2]
    tang = result.ten_du_an.split('||')[1]
    ma_can = result.ten_du_an.split('||')[0]
    dien_tich = result.dien_tich
    return jsonify({
                    'ten_du_an' : ten_du_an,
                    'toa_nha' : toa_nha,
                    'tang' : tang,
                    'ma_can' : ma_can,
                    'dien_tich' : dien_tich,
                    })


# ---------------------- IDTS BT ------------------
@app.route('/ajax_idts_biet_thu_info', methods=['GET', 'POST'])
def ajax_idts_biet_thu_info():
    idts = request.args['idts_bt']
    result = db.session.query(Id_ticket_BT.ten_du_an, Id_ticket_BT.dien_tich_dat, Id_ticket_BT.dien_tich_san_xd).filter_by(id_ticket = idts).distinct().all()[0]
    ten_du_an = result.ten_du_an.split('||')[3]
    duong_nha = result.ten_du_an.split('||')[2]
    loai_nha = result.ten_du_an.split('||')[1]
    ma_can = result.ten_du_an.split('||')[0]
    dien_tich_dat = result.dien_tich_dat
    dien_tich_san_xd = result.dien_tich_san_xd
    return jsonify({
                    'ten_du_an' : ten_du_an,
                    'duong_nha' : duong_nha,
                    'loai_nha' : loai_nha,
                    'ma_can' : ma_can,
                    'dien_tich_dat' : dien_tich_dat,
                    'dien_tich_san_xd' : dien_tich_san_xd,
                    })


# ---------------------- IDTS ND ------------------
@app.route('/ajax_idts_nghi_duong_info', methods=['GET', 'POST'])
def ajax_idts_nghi_duong_info():
    idts = request.args['idts_nd']
    result = db.session.query(Id_ticket_ND.ten_du_an).filter_by(id_ticket = idts).distinct().all()[0]
    ten_du_an = result.ten_du_an.split('||')[3]
    duong_nha = result.ten_du_an.split('||')[2]
    loai_nha = result.ten_du_an.split('||')[1]
    ma_can = result.ten_du_an.split('||')[0]
    return jsonify({
                    'ten_du_an' : ten_du_an,
                    'duong_nha' : duong_nha,
                    'loai_nha' : loai_nha,
                    'ma_can' : ma_can,
                    })


# -------------------- ROA MIN DU AN ----------------
@app.route('/get_du_an_roa_min_du_an',methods=['GET', 'POST'])
def get_du_an_roa_min_du_an():
    ten_du_an = request.args['ten_du_an']
    loai_bds = [r[0] for r in db.session.query(ROA_min_du_an.loai_bds).filter_by(ten_du_an = ten_du_an).distinct().all()]
    return jsonify({'loai_bds' : loai_bds})


@app.route('/get_loai_bds_roa_min_du_an',methods=['GET', 'POST'])
def get_loai_bds_roa_min_du_an():
    ten_du_an = request.args['ten_du_an']
    loai_bds = request.args['loai_bds']
    list_1 = [r[0] for r in db.session.query(ROA_min_du_an.ten_toa_duong_day_khu).filter_by(ten_du_an = ten_du_an, loai_bds = loai_bds).distinct().order_by(ROA_min_du_an.ten_toa_duong_day_khu.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/get_tang_roa_min_du_an',methods=['GET', 'POST'])
def get_tang_roa_min_du_an():
    ten_du_an = request.args['ten_du_an']
    ten_toa_nha = request.args['ten_toa_nha']
    loai_bds = request.args['loai_bds']
    list_1 = [r[0] for r in db.session.query(ROA_min_du_an.ten_tang_loai_nha).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = ten_toa_nha, loai_bds = loai_bds).distinct().order_by(ROA_min_du_an.ten_tang_loai_nha.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/get_ma_can_roa_min_du_an',methods=['GET', 'POST'])
def get_ma_can_roa_min_du_an():
    ten_du_an = request.args['ten_du_an']
    ten_toa_nha = request.args['ten_toa_nha']
    so_tang = request.args['so_tang']
    loai_bds = request.args['loai_bds']
    list_1 = [r[0] for r in db.session.query(ROA_min_du_an.ma_can).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = ten_toa_nha, ten_tang_loai_nha = so_tang, loai_bds = loai_bds).distinct().order_by(ROA_min_du_an.ma_can.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/roa_min_du_an',methods=['GET', 'POST'])
@login_required
def roa_min_du_an():
    ten_du_an = request.args['ten_du_an']
    toa_nha = request.args['toa_nha']
    ten_tang = request.args['ten_tang']
    ma_can = request.args['ma_can']
    loai_bds = request.args['loai_bds']
    result = db.session.query(ROA_min_du_an).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = toa_nha, ten_tang_loai_nha = ten_tang, ma_can = ma_can, loai_bds = loai_bds).all()[0]
    roa = float(result.roa)
    tong_gia_ts = result.tong_gia_ts
    loi_nhuan = roa*float(tong_gia_ts)
    dia_chi = result.dia_chi
    ngay_khoi_tao = dt.datetime.now()
    new_ticket = Ticket_roa_min_du_an(
                                ID_auto_create_roa_min(),
                                ten_du_an,
                                toa_nha,
                                ten_tang,
                                ma_can,
                                roa,
                                dia_chi,
                                session['username'],
                                ngay_khoi_tao,
                                tong_gia_ts,
                                loi_nhuan,
                                loai_bds                                
                                )
    db.session.add(new_ticket)
    db.session.commit()
    return jsonify({
                    'ten_du_an' : ten_du_an,
                    'toa_nha' : toa_nha,
                    'ten_tang' : ten_tang,
                    'ma_can' : ma_can,
                    'dia_chi' : dia_chi,
                    'loai_bds' : loai_bds,
                    'tong_gia_ts' : tong_gia_ts,
                    'roa' : roa,
                    'loi_nhuan' : loi_nhuan,
                    })

    
# ---------------------- ROA MIN THO CU -------------
@app.route('/roa_min_tho_cu',methods=['GET', 'POST'])
@login_required
def roa_min_tho_cu():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    tuyen_duong = request.args['tuyen_duong']
    doan_duong = request.args['doan_duong']
    vi_tri = request.args['vi_tri']
    dien_tich_dat = request.args['dien_tich_dat']
    dien_tich_san_xd = request.args['dien_tich_san_xd']
    loai_nha = request.args['loai_nha']
    thoi_gian_sd = request.args['thoi_gian_sd']

    result = db.session.query(ROA_min_tho_cu).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = tuyen_duong, Doan_duong = doan_duong, Vi_tri = vi_tri).distinct().all()[0]
    try:
        don_gia_loai_nha = db.session.query(Loai_nha.don_gia).filter_by(loai_nha = loai_nha).all()[0].don_gia
    except:
        don_gia_loai_nha = 0
    try:
        ti_le_nam_su_dung = db.session.query(Nam_su_dung.ti_le).filter_by(thoi_gian = thoi_gian_sd).all()[0].ti_le
    except:
        ti_le_nam_su_dung = 0
    dia_chi = doan_duong + ', ' + tuyen_duong + ', ' + quan_huyen + ', ' + tinh_thanh
    don_gia_dat = result.gia_thi_truong
    roa = result.roa

    tong_gia_ts = float(dien_tich_dat)*float(don_gia_dat) + float(dien_tich_san_xd)*float(don_gia_loai_nha)*float(ti_le_nam_su_dung)
    roa_thang = float(roa)
    loi_nhuan = tong_gia_ts*roa_thang
    ngay_khoi_tao = dt.datetime.now()
    new_ticket = Ticket_roa_min_tc(
                                ID_auto_create_roa_min(),
                                doan_duong + '||' + tuyen_duong + '||' + quan_huyen + '||' + tinh_thanh,
                                vi_tri,
                                roa,
                                session['username'],
                                ngay_khoi_tao,
                                tong_gia_ts,
                                loi_nhuan,
                                loai_nha,
                                dien_tich_dat,
                                dien_tich_san_xd,
                                thoi_gian_sd
                                )
    db.session.add(new_ticket)
    db.session.commit()
    return jsonify({
                    'roa_thang' : roa_thang,
                    'tong_gia_ts' : tong_gia_ts,
                    'loi_nhuan' : loi_nhuan,
                    'dia_chi' : dia_chi,
                    'vi_tri' : vi_tri,
                    'loai_nha' : loai_nha,
                    'thoi_gian_sd' : thoi_gian_sd,
                    'dien_tich_dat' : dien_tich_dat,
                    'dien_tich_san_xd' : dien_tich_san_xd,
                    })


@app.route('/get_quan_huyen_roa_min_tc',methods=['GET', 'POST'])
def get_quan_huyen_roa_min_tc():
    tinh_thanh = request.args['tinh_thanh']
    result = [r[0] for r in db.session.query(ROA_min_tho_cu.Quan).filter_by(Tinh_thanh = tinh_thanh).distinct().order_by(ROA_min_tho_cu.Quan.asc()).all()]
    return jsonify({'result' : result})


@app.route('/get_duong_pho_roa_min_tc',methods=['GET', 'POST'])
def get_duong_pho_roa_min_tc():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    result = [r[0] for r in db.session.query(ROA_min_tho_cu.Duong).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen).distinct().order_by(ROA_min_tho_cu.Duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/get_tuyen_duong_roa_min_tc',methods=['GET', 'POST'])
def get_tuyen_duong_roa_min_tc():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    tuyen_duong = request.args['tuyen_duong']
    result = [r[0] for r in db.session.query(ROA_min_tho_cu.Doan_duong).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = tuyen_duong).distinct().order_by(ROA_min_tho_cu.Doan_duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/get_vi_tri_bds_roa_min_tc',methods=['GET', 'POST'])
def get_vi_tri_bds_roa_min_tc():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    tuyen_duong = request.args['tuyen_duong']
    doan_duong = request.args['doan_duong']
    result = [r[0] for r in db.session.query(ROA_min_tho_cu.Vi_tri).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = tuyen_duong, Doan_duong = doan_duong ) .distinct().order_by(ROA_min_tho_cu.Vi_tri.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_dac_diem_vi_tri',methods=['GET', 'POST'])
def ajax_get_dac_diem_vi_tri():
    tinh_thanh = request.args['tinh_thanh']
    vi_tri = request.args['vi_tri']    
    mien = db.session.query(Data_MB.Mien).filter_by(Tinh_thanh = tinh_thanh).distinct().all()[0][0]
    if mien == 'MB':
        yeu_to_loi_the = [r[0] for r in db.session.query(Yeu_to.yeu_to).filter_by(mien = mien, phan_loai = 'LT').distinct().order_by(Yeu_to.yeu_to.asc()).all()]
        yeu_to_bat_loi = [r[0] for r in db.session.query(Yeu_to.yeu_to).filter_by(mien = mien, phan_loai = 'BL').distinct().order_by(Yeu_to.yeu_to.asc()).all()]
    elif mien in ('MN', 'MN1'):
        yeu_to_loi_the = [r[0] for r in db.session.query(Yeu_to.yeu_to).filter_by(mien = mien, vi_tri = vi_tri, phan_loai = 'LT').distinct().order_by(Yeu_to.yeu_to.asc()).all()]        
        yeu_to_bat_loi = [r[0] for r in db.session.query(Yeu_to.yeu_to).filter_by(mien = mien, vi_tri = vi_tri, phan_loai = 'BL').distinct().order_by(Yeu_to.yeu_to.asc()).all()]
    result = [r[0] for r in db.session.query(Dac_diem_VT.dac_diem).filter_by(thanh_pho = tinh_thanh, vi_tri = vi_tri).distinct().order_by(Dac_diem_VT.dac_diem.asc()).all()]
    return jsonify({'result' : result, 'loi_the' : yeu_to_loi_the, 'bat_loi' : yeu_to_bat_loi})


# -------------- BDS BIET THU --------------
@app.route('/ajax_get_option_ten_duong_biet_thu',methods=['GET', 'POST'])
def ajax_get_option_ten_duong_biet_thu():
    ten_du_an = request.args['ten_du_an']
    result = [r[0] for r in db.session.query(BDS_biet_thu.ten_duong).filter_by(ten_du_an = ten_du_an).distinct().order_by(BDS_biet_thu.ten_duong.asc()).all()]
    dia_chi = db.session.query(BDS_biet_thu.dia_chi).filter_by(ten_du_an = ten_du_an).first()
    return jsonify({'result':result, 'dia_chi' : dia_chi})


@app.route('/ajax_get_option_tang_biet_thu',methods=['GET', 'POST'])
def ajax_get_option_tang_biet_thu():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    result = [r[0] for r in db.session.query(BDS_biet_thu.ten_tang).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong).distinct().order_by(BDS_biet_thu.ten_tang.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_option_ma_biet_thu',methods=['GET', 'POST'])
def ajax_get_option_ma_biet_thu():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    ten_tang = request.args['ten_tang']
    list_1 = [r[0] for r in db.session.query(BDS_biet_thu.ma_can).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang).distinct().order_by(BDS_biet_thu.ma_can.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/ajax_get_option_gia_biet_thu',methods=['GET', 'POST'])
@login_required
def ajax_get_option_gia_biet_thu():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    ten_tang = request.args['ten_tang']
    ma_can = request.args['ma_can']
    dien_tich_dat_raw = request.args['dien_tich_dat']
    dien_tich_san_xay_dung_raw = request.args['dien_tich_san_xay_dung']
    if ma_can != '':
        result = db.session.query(BDS_biet_thu.dien_tich_dat, BDS_biet_thu.dien_tich_san_xay_dung, BDS_biet_thu.tong_gia_tri_xay_tho, BDS_biet_thu.tong_gia_tri_hoan_thien, BDS_biet_thu.don_gia_dat, BDS_biet_thu.don_gia_ctxd).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang, ma_can = ma_can).distinct().all()[0]
        dien_tich_dat = result.dien_tich_dat
        dien_tich_san_xay_dung = result.dien_tich_san_xay_dung
        tong_gia_tri_hoan_thien = result.tong_gia_tri_hoan_thien

        don_gia_dat = result.don_gia_dat
        don_gia_ctxd = result.don_gia_ctxd
    if dien_tich_dat_raw != '':
        result = db.session.query(BDS_biet_thu.tong_gia_tri_xay_tho, BDS_biet_thu.don_gia_dat, BDS_biet_thu.don_gia_ctxd).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang).distinct().all()[0]
        dien_tich_dat = dien_tich_dat_raw
        dien_tich_san_xay_dung = dien_tich_san_xay_dung_raw
        don_gia_dat = result.don_gia_dat
        don_gia_ctxd = result.don_gia_ctxd
        if don_gia_dat != 'NULL' and don_gia_ctxd != 'NULL':
            tong_gia_tri_hoan_thien = float(don_gia_dat)*float(dien_tich_dat) + float(don_gia_ctxd)*float(dien_tich_san_xay_dung)
        else:
            if don_gia_dat == 'NULL':
                don_gia_dat = 'Không xác định'
            if don_gia_ctxd == 'NULL':
                don_gia_ctxd = 'Không xác định'
            tong_gia_tri_hoan_thien = 'Không xác định'
    tong_gia_tri_xay_tho = result.tong_gia_tri_xay_tho
    if tong_gia_tri_xay_tho == 'NULL':
        tong_gia_tri_xay_tho = 'Không xác định'
    dia_chi = db.session.query(BDS_biet_thu.dia_chi).filter_by(ten_du_an = ten_du_an).first()
    # NEW TICKET
    dia_chi_cu_the = ma_can + "||" + ten_tang + "||" + ten_duong + "||" + ten_du_an 
    du_lieu_nhap = "|>TT1|" + str(dia_chi_cu_the) + "*|*"
    dien_tich_dat_log = "|>KQ3|" + str(dien_tich_dat) + "*|*"
    dien_tich_san_xay_dung_log = "|>KQ4|" + str(dien_tich_san_xay_dung) + "*|*"
    tong_gia_tri_xay_tho_log = "|>KQ5|" + str(tong_gia_tri_xay_tho) + "*|*"
    tong_gia_tri_hoan_thien_log = "|>KQ6|" + str(tong_gia_tri_hoan_thien) + "*|*"
    don_gia_dat_log = "|>KQ7|" + str(don_gia_dat) + "*|*"
    don_gia_ctxd_log = "|>KQ8|" + str(don_gia_ctxd) + "*|*"
    ket_qua = dien_tich_dat_log + dien_tich_san_xay_dung_log + tong_gia_tri_xay_tho_log + tong_gia_tri_hoan_thien_log + don_gia_dat_log + don_gia_ctxd_log
    ngay_khoi_tao = dt.datetime.now()
    new_id = ID_auto_create()
    new_ticket = Id_ticket_BT(
                new_id,
                dia_chi_cu_the,
                dien_tich_dat,
                dien_tich_san_xay_dung,
                don_gia_dat,
                don_gia_ctxd,
                tong_gia_tri_xay_tho,
                tong_gia_tri_hoan_thien,
                session['username'],
                ngay_khoi_tao,
                dia_chi[0]
            )
    db.session.add(new_ticket)
    # EVENT LOG
    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'bds_biet_thu', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
                    'result':result,
                    'dia_chi' : dia_chi,
                    'ten_du_an' : ten_du_an,
                    'ten_duong' : ten_duong,
                    'ten_tang' : ten_tang,
                    'ma_can' : ma_can,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'new_id' : new_id,
                    'dien_tich_dat' : dien_tich_dat,
                    'dien_tich_san_xay_dung' : dien_tich_san_xay_dung,
                    'tong_gia_tri_xay_tho' : tong_gia_tri_xay_tho,
                    'tong_gia_tri_hoan_thien' : tong_gia_tri_hoan_thien,
                    'don_gia_dat' : don_gia_dat,
                    'don_gia_ctxd' : don_gia_ctxd,
                    })


# -------------- BDS NGHI DUONG --------------
@app.route('/ajax_get_option_ten_duong_nghi_duong',methods=['GET', 'POST'])
def ajax_get_option_ten_duong_nghi_duong():
    ten_du_an = request.args['ten_du_an']
    result = [r[0] for r in db.session.query(BDS_nghi_duong.ten_duong).filter_by(ten_du_an = ten_du_an).distinct().order_by(BDS_nghi_duong.ten_duong.asc()).all()]
    dia_chi = db.session.query(BDS_nghi_duong.dia_chi).filter_by(ten_du_an = ten_du_an).first()
    return jsonify({'result':result, 'dia_chi' : dia_chi})


@app.route('/ajax_get_option_tang_nghi_duong',methods=['GET', 'POST'])
def ajax_get_option_tang_nghi_duong():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    result = [r[0] for r in db.session.query(BDS_nghi_duong.ten_tang).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong).distinct().order_by(BDS_nghi_duong.ten_tang.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_option_ma_nghi_duong',methods=['GET', 'POST'])
def ajax_get_option_ma_nghi_duong():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    ten_tang = request.args['ten_tang']
    list_1 = [r[0] for r in db.session.query(BDS_nghi_duong.ma_can).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang).distinct().order_by(BDS_nghi_duong.ma_can.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/ajax_get_option_gia_nghi_duong',methods=['GET', 'POST'])
@login_required
def ajax_get_option_gia_nghi_duong():
    ten_du_an = request.args['ten_du_an']
    ten_duong = request.args['ten_duong']
    ten_tang = request.args['ten_tang']
    ma_can = request.args['ma_can']
    result = db.session.query(BDS_nghi_duong.dien_tich_dat, BDS_nghi_duong.dien_tich_san_xay_dung, BDS_nghi_duong.tong_gia_tri_xay_tho, BDS_nghi_duong.tong_gia_tri_hoan_thien, BDS_nghi_duong.don_gia_dat, BDS_nghi_duong.don_gia_ctxd).filter_by(ten_du_an = ten_du_an, ten_duong = ten_duong, ten_tang = ten_tang, ma_can = ma_can).distinct().all()[0]
    dien_tich_dat_raw = result.dien_tich_dat
    dien_tich_san_xay_dung_raw = result.dien_tich_san_xay_dung
    tong_gia_tri_xay_tho_raw = result.tong_gia_tri_xay_tho
    tong_gia_tri_hoan_thien_raw = result.tong_gia_tri_hoan_thien
    don_gia_dat_raw = result.don_gia_dat
    don_gia_ctxd_raw = result.don_gia_ctxd
    dia_chi = db.session.query(BDS_nghi_duong.dia_chi).filter_by(ten_du_an = ten_du_an).first()
    # NEW TICKET
    dia_chi_cu_the = ma_can + "||" + ten_tang + "||" + ten_duong + "||" + ten_du_an 
    du_lieu_nhap = "|>TT1|" + str(dia_chi_cu_the) + "*|*"
    dien_tich_dat = "|>KQ3|" + str(result.dien_tich_dat) + "*|*"
    dien_tich_san_xay_dung = "|>KQ4|" + str(result.dien_tich_san_xay_dung) + "*|*"
    tong_gia_tri_xay_tho = "|>KQ5|" + str(result.tong_gia_tri_xay_tho) + "*|*"
    tong_gia_tri_hoan_thien = "|>KQ6|" + str(result.tong_gia_tri_hoan_thien) + "*|*"
    don_gia_dat = "|>KQ7|" + str(result.don_gia_dat) + "*|*"
    don_gia_ctxd = "|>KQ8|" + str(result.don_gia_ctxd) + "*|*"
    ket_qua = dien_tich_dat + dien_tich_san_xay_dung + tong_gia_tri_xay_tho + tong_gia_tri_hoan_thien + don_gia_dat + don_gia_ctxd
    ngay_khoi_tao = dt.datetime.now()
    new_id = ID_auto_create()
    new_ticket = Id_ticket_ND(
                new_id,
                dia_chi_cu_the,
                result.dien_tich_dat,
                result.dien_tich_san_xay_dung,
                result.don_gia_dat,
                result.don_gia_ctxd,
                result.tong_gia_tri_xay_tho,
                result.tong_gia_tri_hoan_thien,
                session['username'],
                ngay_khoi_tao,
                dia_chi[0]
            )
    db.session.add(new_ticket)
    # EVENT LOG
    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'bds_nghi_duong', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
                    'result':result,
                    'dia_chi' : dia_chi,
                    'ten_du_an' : ten_du_an,
                    'ten_duong' : ten_duong,
                    'ten_tang' : ten_tang,
                    'ma_can' : ma_can,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'new_id' : new_id,
                    'dien_tich_dat' : dien_tich_dat_raw,
                    'dien_tich_san_xay_dung' : dien_tich_san_xay_dung_raw,
                    'tong_gia_tri_xay_tho' : tong_gia_tri_xay_tho_raw,
                    'tong_gia_tri_hoan_thien' : tong_gia_tri_hoan_thien_raw,
                    'don_gia_dat' : don_gia_dat_raw,
                    'don_gia_ctxd' : don_gia_ctxd_raw,
                    })


# -------------- BDS CAN HO ----------------
@app.route('/ajax_get_option_du_an_can_ho',methods=['GET', 'POST'])
def ajax_get_option_du_an_can_ho():
    ten_du_an = request.args['ten_du_an']
    result = [r[0] for r in db.session.query(Data_chung_cu.ten_toa_duong_day_khu).filter_by(ten_du_an = ten_du_an).distinct().order_by(Data_chung_cu.ten_toa_duong_day_khu.asc()).all()]
    dia_chi = db.session.query(Data_chung_cu.dia_chi).filter_by(ten_du_an = ten_du_an).first()
    return jsonify({'result' : result, 'dia_chi' : dia_chi})


@app.route('/ajax_get_option_tang_can_ho',methods=['GET', 'POST'])
def ajax_get_option_tang_can_ho():
    ten_du_an = request.args['ten_du_an']
    ten_toa_nha = request.args['ten_toa_nha']
    list_1 = [r[0] for r in db.session.query(Data_chung_cu.ten_tang_loai_nha).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = ten_toa_nha).distinct().order_by(Data_chung_cu.ten_tang_loai_nha.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/ajax_get_option_ma_can_ho',methods=['GET', 'POST'])
def ajax_get_option_ma_can_ho():
    ten_du_an = request.args['ten_du_an']
    ten_toa_nha = request.args['ten_toa_nha']
    so_tang = request.args['so_tang']

    list_1 = [r[0] for r in db.session.query(Data_chung_cu.ma_can).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = ten_toa_nha, ten_tang_loai_nha = so_tang).distinct().order_by(Data_chung_cu.ma_can.asc()).all()]
    result = sorted_nicely(list_1)
    return jsonify({'result':result})


@app.route('/ajax_get_option_gia_can_ho',methods=['GET', 'POST'])
@login_required
def ajax_get_option_gia_can_ho():
    ten_du_an = request.args['ten_du_an']
    ten_toa_nha = request.args['ten_toa_nha']
    so_tang = request.args['so_tang']
    ma_can = request.args['ma_can']
    dien_tich_raw = request.args['dien_tich']

    if ma_can != '':
        result = db.session.query(Data_chung_cu.dien_tich, Data_chung_cu.loai_dien_tich ,Data_chung_cu.don_gia).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = ten_toa_nha, ten_tang_loai_nha = so_tang, ma_can = ma_can).distinct().all()[0]
        dien_tich = result[0]
    else:
        result = db.session.query(Data_chung_cu.loai_dien_tich ,Data_chung_cu.don_gia).filter_by(ten_du_an = ten_du_an, ten_toa_duong_day_khu = ten_toa_nha, ten_tang_loai_nha = so_tang).distinct().all()[0]
        dien_tich = dien_tich_raw
    dia_chi = db.session.query(Data_chung_cu.dia_chi).filter_by(ten_du_an = ten_du_an).first()
    tong_gia = float(result.don_gia)*float(dien_tich)

    dien_tich_log = "|>KQ9|" + str(dien_tich) + "*|*"
    loai_dien_tich_log = "|>KQ10|" + str(result.loai_dien_tich) + "*|*"
    don_gia_log = "|>KQ11|" + str(result.don_gia) + "*|*"
    tong_gia_log = "|>KQ12|" + str(tong_gia) + "*|*"
    dia_chi_cu_the =  ma_can + "||" + so_tang + "||" + ten_toa_nha + "||" + ten_du_an 
    du_lieu_nhap = "|>TT1|" + str(dia_chi_cu_the) + "*|*"
    ket_qua = dien_tich_log + loai_dien_tich_log + don_gia_log + tong_gia_log

    # NEW TICKET
    ngay_khoi_tao = dt.datetime.now()
    new_id = ID_auto_create()
    new_ticket = Id_ticket_CC(
                new_id,
                dia_chi_cu_the,
                dien_tich,
                result.loai_dien_tich,
                result.don_gia,
                tong_gia,
                session['username'],
                ngay_khoi_tao,
                dia_chi[0]
            )

    db.session.add(new_ticket)
    # EVENT LOG
    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'bds_chung_cu', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
                    'result':result,
                    'tong_gia' : tong_gia,
                    'dia_chi' : dia_chi,
                    'ten_du_an' : ten_du_an,
                    'ten_toa_nha' : ten_toa_nha,
                    'so_tang' : so_tang,
                    'ma_can' : ma_can,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'new_id' : new_id,
                    'dien_tich' : dien_tich,
                    'loai_dien_tich' : result.loai_dien_tich,
                    'don_gia' : result.don_gia,
                    })


# ---------------GET OPTION UY BAN-------------
@app.route('/ajax_get_option_quan_huyen_uy_ban',methods=['GET', 'POST'])
def ajax_get_option_quan_huyen_uy_ban():
    tinh_thanh = request.args['tinh_thanh']
    result = [r[0] for r in db.session.query(Khung_gia_uy_ban.quan_huyen).filter_by(thanh_pho = tinh_thanh).distinct().order_by(Khung_gia_uy_ban.quan_huyen.asc()).all()]

    return jsonify({'result' : result})


@app.route('/ajax_get_option_duong_pho_uy_ban',methods=['GET', 'POST'])
def ajax_get_option_duong_pho_uy_ban():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    result = [r[0] for r in db.session.query(Khung_gia_uy_ban.tuyen_duong).filter_by(thanh_pho = tinh_thanh, quan_huyen = quan_huyen).distinct().order_by(Khung_gia_uy_ban.tuyen_duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_option_tuyen_duong_uy_ban',methods=['GET', 'POST'])
def ajax_get_option_tuyen_duong_uy_ban():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    result = [r[0] for r in db.session.query(Khung_gia_uy_ban.doan_tu_den).filter_by(thanh_pho = tinh_thanh, quan_huyen = quan_huyen, tuyen_duong = ten_duong).distinct().order_by(Khung_gia_uy_ban.doan_tu_den.asc()).all()]
    value = db.session.query(Khung_gia_uy_ban.VT1, Khung_gia_uy_ban.VT2, Khung_gia_uy_ban.VT3, Khung_gia_uy_ban.VT4, Khung_gia_uy_ban.VT5).filter_by(thanh_pho = tinh_thanh, quan_huyen = quan_huyen, tuyen_duong = ten_duong).distinct().all()[0]
    return jsonify({'result' : result, 'value' : value})


@app.route('/ajax_get_option_vi_tri_bds_uy_ban',methods=['GET', 'POST'])
def ajax_get_option_vi_tri_bds_uy_ban():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    tuyen_duong = request.args['tuyen_duong']
    result = db.session.query(Khung_gia_uy_ban.VT1, Khung_gia_uy_ban.VT2, Khung_gia_uy_ban.VT3, Khung_gia_uy_ban.VT4, Khung_gia_uy_ban.VT5).filter_by(thanh_pho = tinh_thanh, quan_huyen = quan_huyen, tuyen_duong = ten_duong, doan_tu_den = tuyen_duong).distinct().all()[0]
    return jsonify({'result':result})


# --------------THONG TIN QUY HOACH------------------
@app.route('/ajax_get_option_quan_huyen_quy_hoach',methods=['GET', 'POST'])
def ajax_get_option_quan_huyen_quy_hoach():
    thanh_pho = request.args['thanh_pho']
    result = [r[0] for r in db.session.query(Quy_hoach.quan).filter_by(thanh_pho = thanh_pho).distinct().order_by(Quy_hoach.quan.asc()).all()]
    return jsonify({'result' : result})


@app.route('/ajax_get_option_duong_pho_quy_hoach',methods=['GET', 'POST'])
def ajax_get_option_duong_pho_quy_hoach():
    thanh_pho = request.args['thanh_pho']
    quan_huyen = request.args['quan_huyen']
    result = [r[0] for r in db.session.query(Quy_hoach.duong).filter_by(thanh_pho = thanh_pho, quan = quan_huyen).distinct().order_by(Quy_hoach.duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_option_doan_duong_quy_hoach',methods=['GET', 'POST'])
def ajax_get_option_doan_duong_quy_hoach():
    thanh_pho = request.args['thanh_pho']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    result = [r[0] for r in db.session.query(Quy_hoach.doan_duong).filter_by(thanh_pho = thanh_pho, quan = quan_huyen, duong = ten_duong).distinct().order_by(Quy_hoach.doan_duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_thong_tin_quy_hoach', methods=['GET', 'POST'])
@login_required
def ajax_get_thong_tin_quy_hoach():
    # DATA REQUEST
    thanh_pho = request.args['thanh_pho']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    doan_duong = request.args['doan_duong']
    # DATA RESULT
    result = db.session.query(Quy_hoach.quy_hoach_hien_huu, Quy_hoach.quy_hoach_moi, Quy_hoach.quy_hoach_thoat_lu, Quy_hoach.quy_hoach_cong_trinh).filter_by(thanh_pho = thanh_pho,quan = quan_huyen, duong = ten_duong, doan_duong = doan_duong).distinct().order_by(Quy_hoach.doan_duong.asc()).all()[0]
    ngay_khoi_tao = dt.datetime.now()
    new_id = ID_auto_create()
    dia_chi = doan_duong + '||' + ten_duong + '||' + quan_huyen + '||' + thanh_pho
    quy_hoach_hien_huu = result.quy_hoach_hien_huu
    quy_hoach_moi = result.quy_hoach_moi
    quy_hoach_thoat_lu = result.quy_hoach_thoat_lu
    quy_hoach_cong_trinh = result.quy_hoach_cong_trinh
    ket_qua = quy_hoach_hien_huu + '*|*' + quy_hoach_moi + '*|*' + quy_hoach_thoat_lu + '*|*' + quy_hoach_cong_trinh

    count_quy_hoach_hien_huu = db.session.query(Quy_hoach.quy_hoach_hien_huu).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_hien_huu != "Chưa xác định").count()
    count_quy_hoach_moi = db.session.query(Quy_hoach.quy_hoach_moi).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_moi != "Chưa xác định").count()
    count_quy_hoach_thoat_lu = db.session.query(Quy_hoach.quy_hoach_thoat_lu).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_thoat_lu != "Chưa xác định").count()
    count_quy_hoach_cong_trinh = db.session.query(Quy_hoach.quy_hoach_cong_trinh).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_cong_trinh != "Chưa xác định").count()

    list_count_quy_hoach = [count_quy_hoach_hien_huu, count_quy_hoach_moi, count_quy_hoach_thoat_lu, count_quy_hoach_cong_trinh]

    count_all_quy_hoach = sum(list_count_quy_hoach)
    try:
        list_percent_quy_hoach = [
                             count_quy_hoach_hien_huu/count_all_quy_hoach*100,
                             count_quy_hoach_moi/count_all_quy_hoach*100,
                             count_quy_hoach_thoat_lu/count_all_quy_hoach*100,
                             count_quy_hoach_cong_trinh/count_all_quy_hoach*100
                             ]
    except:
        list_percent_quy_hoach = [0, 0, 0, 0]
    list_label = ["Quy hoạch mở đường " + "<br>" + " hiện hữu",
                "Quy hoạch " + "<br>" + "mở đường mới" ,
                "Quy hoạch hành lang " + "<br>" + " thoát lũ",
                "Quy hoạch liên quan đến " + "<br>" + " các công trình công cộng, " + "<br>" + " công trình Nhà nước"]
    dict_label_value = [{"label" : list_label[i], "value" : r} for i,r in enumerate(list_percent_quy_hoach) if r != 0]
    # NEW TICKET
    new_ticket = Id_ticket_quy_hoach(
                                    new_id,
                                    dia_chi,
                                    '',
                                    ket_qua,
                                    session['username'],
                                    ngay_khoi_tao
                                    )
    db.session.add(new_ticket)

    # LOG
    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'quy_hoach', dia_chi, ket_qua)
    db.session.add(event_log)
    db.session.commit()

    return jsonify({
                    'new_id' : new_id,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'dia_chi' : dia_chi,
                    'quy_hoach_hien_huu' : quy_hoach_hien_huu,
                    'quy_hoach_moi' : quy_hoach_moi,
                    'quy_hoach_thoat_lu' : quy_hoach_thoat_lu,
                    'quy_hoach_cong_trinh' : quy_hoach_cong_trinh,
                    'list_percent_quy_hoach' : list_percent_quy_hoach,
                    'dict_label_value' : dict_label_value,
                    'thanh_pho' : thanh_pho,
                    'quan_huyen' : quan_huyen,
                    })


# -------------- BDS NHA THO CU ------------
@app.route('/ajax_get_option_quan_huyen',methods=['GET', 'POST'])
def ajax_get_option_quan_huyen():
    tinh_thanh = request.args['tinh_thanh']
    result = [r[0] for r in db.session.query(Data_MB.Quan).filter_by(Tinh_thanh = tinh_thanh).distinct().order_by(Data_MB.Quan.asc()).all()]
    mien = [r[0] for r in db.session.query(Data_MB.Mien).filter_by(Tinh_thanh = tinh_thanh).distinct().order_by(Data_MB.Mien.asc()).all()]
    return jsonify({'result' : result, 'mien' : mien})


@app.route('/ajax_get_option_duong_pho',methods=['GET', 'POST'])
def ajax_get_option_duong_pho():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    result = [r[0] for r in db.session.query(Data_MB.Duong).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen).distinct().order_by(Data_MB.Duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_option_tuyen_duong',methods=['GET', 'POST'])
def ajax_get_option_tuyen_duong():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    result = [r[0] for r in db.session.query(Data_MB.Doan_duong).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = ten_duong).distinct().order_by(Data_MB.Doan_duong.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_option_vi_tri_bds',methods=['GET', 'POST'])
def ajax_get_option_vi_tri_bds():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    tuyen_duong = request.args['tuyen_duong']
    result = [r[0] for r in db.session.query(Data_MB.Vi_tri).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = ten_duong, Doan_duong = tuyen_duong ) .distinct().order_by(Data_MB.Vi_tri.asc()).all()]
    return jsonify({'result':result})


@app.route('/ajax_get_gia_uy_ban', methods=['GET', 'POST'])
def ajax_get_gia_uy_ban():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    ten_duong = request.args['ten_duong']
    tuyen_duong = request.args['tuyen_duong']
    vi_tri_bds = request.args['vi_tri_bds']    
    result = db.session.query(Data_MB.Thong_tin_quy_hoach, Data_MB.Gia_thi_truong).filter_by(Tinh_thanh = tinh_thanh,Quan = quan_huyen, Duong = ten_duong, Doan_duong = tuyen_duong, Vi_tri = vi_tri_bds).distinct().order_by(Data_MB.Doan_duong.asc()).all()[0]
    return jsonify({'result' : result})


# ----------------- ROA THO CU ----------------
@app.route('/ajax_idts_tho_cu_info',methods=['GET', 'POST'])
def ajax_idts_tho_cu_info():
    idts = request.args['idts_tc']
    result = db.session.query(Id_ticket.dia_chi, Id_ticket.vi_tri).filter_by(id_ticket = idts).distinct().all()[0]
    dia_chi = result.dia_chi
    vi_tri = result.vi_tri
    return jsonify({'dia_chi' : dia_chi, 'vi_tri' : vi_tri})


@app.route('/ajax_roa_tho_cu',methods=['GET', 'POST'])
@login_required
def ajax_roa_tho_cu():
    tinh_thanh = request.args['tinh_thanh']
    quan_huyen = request.args['quan_huyen']
    tuyen_duong = request.args['tuyen_duong']
    doan_duong = request.args['doan_duong']
    vi_tri = request.args['vi_tri']
    idts = request.args['idts_tc']
    tong_gia_ts = db.session.query(Event_log.ket_qua).filter_by(id_ticket = idts).all()[0]
    tong_gia_ts = tong_gia_ts.ket_qua.split("|>KQ16|")[1].split("*|*")[0]
    roa = db.session.query(ROA_tho_cu.roa8).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = tuyen_duong, Doan_duong = doan_duong, Vi_tri = vi_tri).distinct().all()[0].roa8
    new_id = ID_auto_create()
    roa = round(float(roa), 4)
    result = round(roa * float(tong_gia_ts), 3)
    dia_chi = doan_duong + "||" + tuyen_duong + "||" + quan_huyen + "||" + tinh_thanh
    ngay_khoi_tao = dt.datetime.now()
    dia_chi_roa_tho_cu_log = "|>TT13|" + str(dia_chi) + "*|*"
    idts_roa_tho_cu_log = "|>TT14|" + str(idts) + "*|*"
    vi_tri_roa_tho_cu_log = "|>TT15|" + str(vi_tri) + "*|*"
    roa_tho_cu_log = "|>KQ17|" + str(roa) + "*|*"
    tong_gia_ts_roa_tho_cu_log = "|>KQ18|" + str(tong_gia_ts) + "*|*"
    loi_nhuan_roa_tho_cu_log = "|>KQ19|" + str(result) + "*|*"
    du_lieu_nhap = dia_chi_roa_tho_cu_log + idts_roa_tho_cu_log + vi_tri_roa_tho_cu_log
    ket_qua = roa_tho_cu_log + tong_gia_ts_roa_tho_cu_log + loi_nhuan_roa_tho_cu_log
    new_ticket = Ticket_roa_tc(
    							new_id,
    							dia_chi,
    							vi_tri,
    							roa,
    							session['username'],
    							ngay_khoi_tao,
    							idts,
    							tong_gia_ts,
    							result
    							)
    db.session.add(new_ticket)
    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'roa_bds_tho_cu', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    db.session.commit()
    return jsonify({
    				'roa' : roa,
    				'result' : result,
    				'new_id' : new_id,
    				'idts' : idts,
    				'tong_gia_ts' : tong_gia_ts,
    				'dia_chi' : dia_chi,
    				'ngay_khoi_tao' : ngay_khoi_tao,
    				'vi_tri' : vi_tri,
    				})


# ---------------- SEARCH TICKET ----------------
@app.route('/ajax_search_ticket', methods=['GET', 'POST'])
def ajax_search_ticket():
    id_ticket = request.args['id_ticket']
    phan_loai = db.session.query(Event_log.phan_loai).filter_by(id_ticket = id_ticket).all()[0].phan_loai
    if phan_loai == 'bds_tho_cu':
        result = db.session.query(Id_ticket.id_ticket, Id_ticket.dia_chi, Id_ticket.vi_tri, Id_ticket.dien_tich, Id_ticket.mat_tien, Id_ticket.hinh_dang, Id_ticket.do_rong_ngo, Id_ticket.kcach_truc_chinh, Id_ticket.yeu_to_loi_the, Id_ticket.yeu_to_bat_loi, Id_ticket.gia_truoc, Id_ticket.gia_sau, Id_ticket.loai_nha, Id_ticket.thoi_gian_su_dung, Id_ticket.don_gia_ctxd, Id_ticket.dien_tich_san_xd, Id_ticket.username, Id_ticket.time).filter_by(id_ticket = id_ticket).all()[0]
        thanh_pho = result.dia_chi.split("||")[-1]
        vi_tri = result.vi_tri
        try:
            dac_diem_VT = db.session.query(Dac_diem_VT.dac_diem).filter_by(thanh_pho = thanh_pho, vi_tri = vi_tri).all()[0].dac_diem
        except:
            dac_diem_VT = ''
        return jsonify({'result' : result, 'phan_loai' : phan_loai, 'dac_diem_VT': dac_diem_VT})
    if phan_loai == 'quy_hoach':
        result = db.session.query(Id_ticket_quy_hoach.id_ticket, Id_ticket_quy_hoach.dia_chi, Id_ticket_quy_hoach.vi_tri, Id_ticket_quy_hoach.ket_qua, Id_ticket_quy_hoach.username, Id_ticket_quy_hoach.time).filter_by(id_ticket = id_ticket).all()[0]
        thanh_pho = result.dia_chi.split("||")[-1]
        quan_huyen = result.dia_chi.split("||")[-2]
        count_quy_hoach_hien_huu = db.session.query(Quy_hoach.quy_hoach_hien_huu).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_hien_huu != "Chưa xác định").count()
        count_quy_hoach_moi = db.session.query(Quy_hoach.quy_hoach_moi).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_moi != "Chưa xác định").count()
        count_quy_hoach_thoat_lu = db.session.query(Quy_hoach.quy_hoach_thoat_lu).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_thoat_lu != "Chưa xác định").count()
        count_quy_hoach_cong_trinh = db.session.query(Quy_hoach.quy_hoach_cong_trinh).filter_by(thanh_pho = thanh_pho,quan = quan_huyen).filter(Quy_hoach.quy_hoach_cong_trinh != "Chưa xác định").count()
        list_count_quy_hoach = [count_quy_hoach_hien_huu, count_quy_hoach_moi, count_quy_hoach_thoat_lu, count_quy_hoach_cong_trinh]
        count_all_quy_hoach = sum(list_count_quy_hoach)
        try:
            list_percent_quy_hoach = [
                                 count_quy_hoach_hien_huu/count_all_quy_hoach*100,
                                 count_quy_hoach_moi/count_all_quy_hoach*100,
                                 count_quy_hoach_thoat_lu/count_all_quy_hoach*100,
                                 count_quy_hoach_cong_trinh/count_all_quy_hoach*100
                                 ]
        except:
            list_percent_quy_hoach = [0, 0, 0, 0]
        list_label = ["Quy hoạch mở đường " + "<br>" + " hiện hữu",
                    "Quy hoạch " + "<br>" + "mở đường mới" ,
                    "Quy hoạch hành lang " + "<br>" + " thoát lũ",
                    "Quy hoạch liên quan đến " + "<br>" + " các công trình công cộng, " + "<br>" + " công trình Nhà nước"]
        dict_label_value = [{"label" : list_label[i], "value" : r} for i,r in enumerate(list_percent_quy_hoach) if r != 0]        
        return jsonify({'result' : result, 'phan_loai' : phan_loai, 'dict_label_value' : dict_label_value})

    if phan_loai == 'bds_chung_cu':
        result = db.session.query(Id_ticket_CC.id_ticket, Id_ticket_CC.ten_du_an, Id_ticket_CC.dien_tich, Id_ticket_CC.loai_dien_tich, Id_ticket_CC.don_gia, Id_ticket_CC.tong_gia, Id_ticket_CC.username, Id_ticket_CC.time, Id_ticket_CC.dia_chi).filter_by(id_ticket = id_ticket).all()[0]

    if phan_loai == 'bds_biet_thu':
        result = db.session.query(Id_ticket_BT.id_ticket, Id_ticket_BT.ten_du_an, Id_ticket_BT.dien_tich_dat, Id_ticket_BT.dien_tich_san_xd, Id_ticket_BT.don_gia_dat, Id_ticket_BT.don_gia_ctxd, Id_ticket_BT.tong_gia_xay_tho, Id_ticket_BT.tong_gia_hoan_thien, Id_ticket_BT.username, Id_ticket_BT.time,  Id_ticket_BT.dia_chi).filter_by(id_ticket = id_ticket).all()[0]
    if phan_loai == 'bds_nghi_duong':
        result = db.session.query(Id_ticket_ND.id_ticket, Id_ticket_ND.ten_du_an, Id_ticket_ND.dien_tich_dat, Id_ticket_ND.dien_tich_san_xd, Id_ticket_ND.don_gia_dat, Id_ticket_ND.don_gia_ctxd, Id_ticket_ND.tong_gia_xay_tho, Id_ticket_ND.tong_gia_hoan_thien, Id_ticket_ND.username, Id_ticket_ND.time,  Id_ticket_ND.dia_chi).filter_by(id_ticket = id_ticket).all()[0]
    if phan_loai == 'roa_bds_tho_cu':
    	result = db.session.query(Ticket_roa_tc.id_ticket, Ticket_roa_tc.dia_chi, Ticket_roa_tc.roa, Ticket_roa_tc.time, Ticket_roa_tc.idts, Ticket_roa_tc.tong_gia_ts, Ticket_roa_tc.loi_nhuan, Ticket_roa_tc.vi_tri).filter_by(id_ticket = id_ticket).all()[0]
    if phan_loai == 'roa_bds_nghi_duong':
    	result = db.session.query(Ticket_roa_nd.id_ticket, Ticket_roa_nd.dia_chi, Ticket_roa_nd.roa, Ticket_roa_nd.time, Ticket_roa_nd.ten_du_an, Ticket_roa_nd.tong_gia_ts, Ticket_roa_nd.loi_nhuan, Ticket_roa_nd.ten_duong, Ticket_roa_nd.ten_tang, Ticket_roa_nd.ma_can).filter_by(id_ticket = id_ticket).all()[0]

    if phan_loai == 'roa_bds_biet_thu':
    	result = db.session.query(Ticket_roa_bt.id_ticket, Ticket_roa_bt.dia_chi, Ticket_roa_bt.roa, Ticket_roa_bt.time, Ticket_roa_bt.ten_du_an, Ticket_roa_bt.tong_gia_ts, Ticket_roa_bt.loi_nhuan, Ticket_roa_bt.ten_duong, Ticket_roa_bt.ten_tang, Ticket_roa_bt.ma_can).filter_by(id_ticket = id_ticket).all()[0]
    if phan_loai == 'roa_bds_chung_cu':
        result = db.session.query(Ticket_roa_cc.id_ticket, Ticket_roa_cc.dia_chi, Ticket_roa_cc.roa, Ticket_roa_cc.time, Ticket_roa_cc.ten_du_an, Ticket_roa_cc.tong_gia_ts, Ticket_roa_cc.loi_nhuan, Ticket_roa_cc.don_gia, Ticket_roa_cc.dien_tich).filter_by(id_ticket = id_ticket).all()[0]
    return jsonify({'result' : result, 'phan_loai' : phan_loai})


# GET RESULT
def he_so_dieu_chinh(value,percent):
    if percent == 0:
        return 0
    else:
        return float(value)*(1-float(percent))/float(percent)


# TINH HE SO DIEN TICH
def quy_mo(x, mien, vi_tri):
    if x == 0 or x == '':
        return 0
    if mien == 'MB':
        quy_mo_data = db.session.query(Quy_mo).filter_by(mien = mien).order_by(Quy_mo.quy_mo.asc()).all()
    elif mien in ('MN', 'MN1'):
        quy_mo_data = db.session.query(Quy_mo).filter_by(mien = mien, vi_tri = vi_tri).order_by(Quy_mo.quy_mo.asc()).all()
    quy_mo_data = sorted(quy_mo_data, key=lambda x: float(x.quy_mo))
    for r in range(len(quy_mo_data)):
        data_0 = float(quy_mo_data[r].quy_mo)
        data_1 = float(quy_mo_data[r].ti_le)
        data_2 = float(quy_mo_data[r+1].ti_le)
        data_3 = float(quy_mo_data[r+1].quy_mo)
        data_4 = float(quy_mo_data[-1].quy_mo)
        data_5 = float(quy_mo_data[-1].ti_le)
        if x < data_0:
            return data_1
        elif x == data_0:
            return data_1
        elif data_0 < x < data_3:
            y = data_2 - (data_3 - x)*(data_2 - data_1)/(data_3 - data_0)
            return y
        elif x > data_4:
            return data_5


# HE SO MAT TIEN
def mat_tien(x, mien, vi_tri):
    if x == 0 or x == '':
        return 0
    if mien == 'MB':
        mat_tien_data = db.session.query(Mat_tien).filter_by(mien = mien).order_by(Mat_tien.mat_tien.asc()).all()
    elif mien in ('MN', 'MN1'):
        mat_tien_data = db.session.query(Mat_tien).filter_by(mien = mien, vi_tri = vi_tri).order_by(Mat_tien.mat_tien.asc()).all()
    mat_tien_data = sorted(mat_tien_data, key=lambda x: float(x.mat_tien))
    for r in range(len(mat_tien_data)):
        if x < float(mat_tien_data[r].mat_tien):
            return mat_tien_data[r].ti_le
        elif x == float(mat_tien_data[r].mat_tien):
            return mat_tien_data[r].ti_le
        elif float(mat_tien_data[r].mat_tien) < x < float(mat_tien_data[r+1].mat_tien):
            y = float(mat_tien_data[r+1].ti_le) - ((float(mat_tien_data[r+1].mat_tien) - x)*(float(mat_tien_data[r+1].ti_le) - float(mat_tien_data[r].ti_le))/(float(mat_tien_data[r+1].mat_tien) - float(mat_tien_data[r].mat_tien))) 
            return y
        elif x > float(mat_tien_data[-1].mat_tien):
            return mat_tien_data[-1].ti_le


#DO RONG NGO
def do_rong_ngo(a, b, x, mien):
    x = float(x)
    if x == 0 or x == '':
        return 0
    do_rong_ngo_data = db.session.query(Do_rong_ngo).filter_by(Tinh_thanh = a, vi_tri = b).all()
    if not do_rong_ngo_data and mien == 'MB':
        do_rong_ngo_data = db.session.query(Do_rong_ngo).filter_by(Tinh_thanh = 'TP. Hà Nội', vi_tri = b).all()
    elif not do_rong_ngo_data and mien == 'MN':
        do_rong_ngo_data = db.session.query(Do_rong_ngo).filter_by(Tinh_thanh = 'TP. Hồ Chí Minh', vi_tri = b).all()
    do_rong_ngo_data = sorted(do_rong_ngo_data, key=lambda x: float(x.khoang_cach))
    for r in range(len(do_rong_ngo_data)):
        if x < float(do_rong_ngo_data[r].khoang_cach):
            return do_rong_ngo_data[r].ti_le
        elif x == float(do_rong_ngo_data[r].khoang_cach):
            return do_rong_ngo_data[r].ti_le
        elif float(do_rong_ngo_data[r].khoang_cach) < x < float(do_rong_ngo_data[r+1].khoang_cach):
            y = float(do_rong_ngo_data[r+1].ti_le) - ((float(do_rong_ngo_data[r+1].khoang_cach) - x)*(float(do_rong_ngo_data[r+1].ti_le) - float(do_rong_ngo_data[r].ti_le))/(float(do_rong_ngo_data[r+1].khoang_cach) - float(do_rong_ngo_data[r].khoang_cach)))
            return y
        elif x > float(do_rong_ngo_data[len(do_rong_ngo_data)-1].khoang_cach):
            return do_rong_ngo_data[len(do_rong_ngo_data)-1].ti_le


#KC TRUC CHINH  
def khoang_cach_den_truc_chinh(a, b, x, mien):
    x = float(x)
    if x == 0 or x == '':
        return 0
    khoang_cach_den_truc_chinh_data = db.session.query(Khoang_cach_truc).filter_by(Tinh_thanh = a, vi_tri = b).order_by(Khoang_cach_truc.khoang_cach.asc()).all()
    if not khoang_cach_den_truc_chinh_data and mien == 'MB':
        khoang_cach_den_truc_chinh_data = db.session.query(Khoang_cach_truc).filter_by(Tinh_thanh = 'TP. Hà Nội', vi_tri = b).order_by(Khoang_cach_truc.khoang_cach.asc()).all()
    elif not khoang_cach_den_truc_chinh_data and mien == 'MN':
        khoang_cach_den_truc_chinh_data = db.session.query(Khoang_cach_truc).filter_by(Tinh_thanh = 'TP. Hồ Chí Minh', vi_tri = b).order_by(Khoang_cach_truc.khoang_cach.asc()).all()
    khoang_cach_den_truc_chinh_data = sorted(khoang_cach_den_truc_chinh_data, key=lambda x: float(x.khoang_cach))
    for r in range(len(khoang_cach_den_truc_chinh_data)):
        if x < float(khoang_cach_den_truc_chinh_data[r].khoang_cach):
            return khoang_cach_den_truc_chinh_data[r].ti_le
        elif x == float(khoang_cach_den_truc_chinh_data[r].khoang_cach):
            return khoang_cach_den_truc_chinh_data[r].ti_le
        elif float(khoang_cach_den_truc_chinh_data[r].khoang_cach) < x < float(khoang_cach_den_truc_chinh_data[r+1].khoang_cach):
            y = float(khoang_cach_den_truc_chinh_data[r+1].ti_le) - ((float(khoang_cach_den_truc_chinh_data[r+1].khoang_cach) - x)*(float(khoang_cach_den_truc_chinh_data[r+1].ti_le) - float(khoang_cach_den_truc_chinh_data[r].ti_le))/(float(khoang_cach_den_truc_chinh_data[r+1].khoang_cach) - float(khoang_cach_den_truc_chinh_data[r].khoang_cach))) 
            return y
        elif x > float(khoang_cach_den_truc_chinh_data[-1].khoang_cach):
            return khoang_cach_den_truc_chinh_data[-1].ti_le


# GET TINH THANH THO CU
@app.route('/get_tinh_thanh_tho_cu', methods=['GET', 'POST'])
def get_tinh_thanh_tho_cu():
    list_tinh_thanh = [r[0] for r in db.session.query(Data_MB.Tinh_thanh).distinct().order_by(Data_MB.Tinh_thanh.asc()).all()]
    return jsonify({'result' : list_tinh_thanh})


# GET DU AN CC - BT - NGHI DUONG
@app.route('/get_du_an_chung_cu', methods=['GET', 'POST'])
def get_du_an_chung_cu():
    list_can_ho = [r[0] for r in db.session.query(Data_chung_cu.ten_du_an).distinct().order_by(Data_chung_cu.ten_du_an.asc()).all()]
    return jsonify({'result' : list_can_ho})


@app.route('/get_du_an_biet_thu', methods=['GET', 'POST'])
def get_du_an_biet_thu():
    list_biet_thu = [r[0] for r in db.session.query(BDS_biet_thu.ten_du_an).distinct().order_by(BDS_biet_thu.ten_du_an.asc()).all()]
    return jsonify({'result' : list_biet_thu})


@app.route('/get_du_an_nghi_duong', methods=['GET', 'POST'])
def get_du_an_nghi_duong():
    list_nghi_duong = [r[0] for r in db.session.query(BDS_nghi_duong.ten_du_an).distinct().order_by(BDS_nghi_duong.ten_du_an.asc()).all()]
    return jsonify({'result' : list_nghi_duong})


@app.route('/get_tinh_thanh_roa_min', methods=['GET', 'POST'])
def get_tinh_thanh_roa_min():
    list_tinh_thanh_roa_tsdb = [r[0] for r in db.session.query(ROA_min_tho_cu.Tinh_thanh).distinct().order_by(ROA_min_tho_cu.Tinh_thanh.asc()).all()]
    return jsonify({'result' : list_tinh_thanh_roa_tsdb})


@app.route('/get_du_an_roa_min', methods=['GET', 'POST'])
def get_du_an_roa_min():
    list_du_an_roa_tsdb = [r[0] for r in db.session.query(ROA_min_du_an.ten_du_an).distinct().order_by(ROA_min_du_an.ten_du_an.asc()).all()]
    return jsonify({'result' : list_du_an_roa_tsdb})


@app.route('/get_idts_roa_tho_cu', methods=['GET', 'POST'])
def get_idts_roa_tho_cu():
    Id_ticket_roa_tho_cu = [r[0] for r in db.session.query(Event_log.id_ticket).filter_by(phan_loai = 'bds_tho_cu').order_by(Event_log.id_ticket.desc()).all()]
    return jsonify({'result' : Id_ticket_roa_tho_cu})


@app.route('/get_idts_roa_chung_cu', methods=['GET', 'POST'])
def get_idts_roa_chung_cu():
    Id_ticket_roa_chung_cu = [r[0] for r in db.session.query(Event_log.id_ticket).filter_by(phan_loai = 'bds_chung_cu').order_by(Event_log.id_ticket.desc()).all()]
    return jsonify({'result' : Id_ticket_roa_chung_cu})


@app.route('/get_idts_roa_biet_thu', methods=['GET', 'POST'])
def get_idts_roa_biet_thu():
    Id_ticket_roa_biet_thu = [r[0] for r in db.session.query(Event_log.id_ticket).filter_by(phan_loai = 'bds_biet_thu').order_by(Event_log.id_ticket.desc()).all()]
    return jsonify({'result' : Id_ticket_roa_biet_thu})


@app.route('/get_idts_roa_nghi_duong', methods=['GET', 'POST'])
def get_idts_roa_nghi_duong():
    Id_ticket_roa_nghi_duong = [r[0] for r in db.session.query(Event_log.id_ticket).filter_by(phan_loai = 'bds_nghi_duong').order_by(Event_log.id_ticket.desc()).all()]
    return jsonify({'result' : Id_ticket_roa_nghi_duong})
  

@app.route('/ajax_get_result', methods=['GET', 'POST'])
@login_required
def ajax_get_result():
    # GET DATA
    tinh_thanh = request.args['tinh_thanh_thi_truong']
    quan_huyen = request.args['quan_huyen_thi_truong']
    ten_duong = request.args['ten_duong_thi_truong']
    tuyen_duong = request.args['tuyen_duong_thi_truong']
    vi_tri_bds = request.args['vi_tri_bds_thi_truong']
    try:
        do_rong_ngo_thi_truong = float(request.args['do_rong_ngo_thi_truong'])
    except:
        do_rong_ngo_thi_truong = 0
    try:
        kcach_truc_chinh_thi_truong = float(request.args['kcach_truc_chinh_thi_truong'])
    except:
        kcach_truc_chinh_thi_truong = 0
    try:
        dien_tich_dat_thi_truong = float(request.args['dien_tich_dat_thi_truong'])
    except:
        dien_tich_dat_thi_truong = 0
    try:
        do_rong_mat_tien_thi_truong = float(request.args['do_rong_mat_tien_thi_truong'])
    except:
        do_rong_mat_tien_thi_truong = 0
    hinh_dang = request.args['hinh_dang']
    loai_nha_tho_cu = request.args['loai_nha_tho_cu']
    try:
        thoi_gian_su_dung = request.args['thoi_gian_su_dung']
    except:
        thoi_gian_su_dung = 'Chưa xác định'
    try:
        dien_tich_san_xd = request.args['dien_tich_san_xd']
    except:
        dien_tich_san_xd = 0
    try:
        tang_ham_ctxd = request.args['tang_ham_ctxd']
    except:
        tang_ham_ctxd = 0
    try:
        don_gia_loai_nha = db.session.query(Loai_nha.don_gia).filter_by(loai_nha = loai_nha_tho_cu).all()[0].don_gia
    except:
        don_gia_loai_nha = 0
    try:
        ti_le_nam_su_dung = db.session.query(Nam_su_dung.ti_le).filter_by(thoi_gian = thoi_gian_su_dung).all()[0].ti_le
    except:
        ti_le_nam_su_dung = 0
    list_yeu_to = request.args['data_yeu_to'].split("|")
    list_loi_the = request.args['data_loi_the'].split("|")
    list_bat_loi = request.args['data_bat_loi'].split("|")
    try:
        dac_diem_VT = db.session.query(Dac_diem_VT.dac_diem).filter_by(thanh_pho = tinh_thanh, vi_tri = vi_tri_bds).all()[0].dac_diem
    except:
        dac_diem_VT = ''

    # TINH HE SO
    gia_tri_du_lieu = db.session.query(Data_MB.Gia_thi_truong, Data_MB.Mien, Data_MB.Dia_chi).filter_by(Tinh_thanh = tinh_thanh, Quan = quan_huyen, Duong = ten_duong, Doan_duong = tuyen_duong, Vi_tri = vi_tri_bds).all()

    gia_thi_truong = int(gia_tri_du_lieu[0][0].split(".")[0])
    mien = gia_tri_du_lieu[0][1]
    dia_chi = gia_tri_du_lieu[0][2]


    he_so_dien_tich = quy_mo(dien_tich_dat_thi_truong, mien, vi_tri_bds)
    he_so_mat_tien = mat_tien(do_rong_mat_tien_thi_truong, mien, vi_tri_bds)
    if vi_tri_bds in ['Vị trí 2', 'Vị trí 3', 'Vị trí 4']:
        he_so_rong_ngo = do_rong_ngo(tinh_thanh, vi_tri_bds, do_rong_ngo_thi_truong, mien)
        he_so_kc_truc_chinh = khoang_cach_den_truc_chinh(tinh_thanh, vi_tri_bds, kcach_truc_chinh_thi_truong, mien)
    else:
        he_so_rong_ngo = 0
        he_so_kc_truc_chinh = 0

    try:
        if mien == 'MB':
            vi_tri = 'Vị trí 0'
            he_so_hinh_dang = float(db.session.query(Hinh_dang).filter_by(hinh_dang = hinh_dang, mien = mien).all()[0].ti_le)

        elif mien in ('MN', 'MN1'):
            vi_tri = vi_tri_bds
            he_so_hinh_dang = float(db.session.query(Hinh_dang).filter_by(hinh_dang = hinh_dang, mien = mien, vi_tri = vi_tri_bds).all()[0].ti_le)     

    except:
        he_so_hinh_dang = 0

    # TINH GIA CTXD
    don_gia_ctxd = float(don_gia_loai_nha)
    # don_gia_ctxd = float(don_gia_loai_nha)*float(ti_le_nam_su_dung)
    # if tang_ham_ctxd == 'Có':
    #     don_gia_ctxd = float(don_gia_loai_nha)*float(ti_le_nam_su_dung)*1.5
    
    try:
        tong_gia_ctxd = float(dien_tich_san_xd)*don_gia_ctxd*float(ti_le_nam_su_dung)
    except:
        tong_gia_ctxd = 0

    if tang_ham_ctxd == 'Có':
        tong_gia_ctxd = tong_gia_ctxd + 400000000
    # TINH GIA DIEU CHINH DAT
    gia_tri_dieu_chinh_dien_tich = he_so_dieu_chinh(gia_thi_truong, he_so_dien_tich)
    gia_tri_dieu_chinh_mat_tien = he_so_dieu_chinh(gia_thi_truong, he_so_mat_tien)
    gia_tri_dieu_chinh_hinh_dang = he_so_dieu_chinh(gia_thi_truong, he_so_hinh_dang)
    if list_yeu_to != [u'']:
        gia_tri_dieu_chinh_yeu_to = sum([he_so_dieu_chinh(gia_thi_truong, db.session.query(Yeu_to.ti_le).filter_by(yeu_to = r, mien = mien, vi_tri = vi_tri).all()[0].ti_le) for r in list_yeu_to])
    else:
        gia_tri_dieu_chinh_yeu_to = 0
    gia_dieu_chinh_rong_ngo = he_so_dieu_chinh(gia_thi_truong, he_so_rong_ngo)
    gia_dieu_kc_truc_chinh = he_so_dieu_chinh(gia_thi_truong, he_so_kc_truc_chinh)
    # SUM GIA
    gia_dieu_chinh = round(sum([gia_tri_dieu_chinh_dien_tich,
                        gia_tri_dieu_chinh_mat_tien,
                        gia_tri_dieu_chinh_hinh_dang,
                        gia_dieu_chinh_rong_ngo,
                        gia_dieu_kc_truc_chinh,
                        gia_tri_dieu_chinh_yeu_to]) + gia_thi_truong)
    tong_gia_dat = float(gia_dieu_chinh)*float(dien_tich_dat_thi_truong)
    tong_gia_tai_san = tong_gia_dat + tong_gia_ctxd
    

    # EVENT LOG
    ngay_khoi_tao = dt.datetime.now()
    new_id = ID_auto_create()
    dia_chi_cu_the = str(tuyen_duong + '||' +  ten_duong + '||' + quan_huyen + '||' + tinh_thanh)
    # DU LIEU NHAP
    dia_chi_log = "|>TT1|" + dia_chi_cu_the + "*|*"
    vi_tri_bds_log = "|>TT2|" + str(vi_tri_bds) + "*|*" 
    dien_tich_log = "|>TT3|" + str(dien_tich_dat_thi_truong) + "*|*" 
    mat_tien_log = "|>TT4|" + str(do_rong_mat_tien_thi_truong) + "*|*" 
    hinh_dang_log = "|>TT5|" + str(hinh_dang) + "*|*" 
    do_rong_ngo_log = "|>TT6|" + str(do_rong_ngo_thi_truong) + "*|*" 
    truc_chinh_log = "|>TT7|" + str(kcach_truc_chinh_thi_truong) + "*|*" 
    loi_the_log = "|>TT8|" + str("|".join(list_loi_the)) + "*|*" 
    bat_loi_log = "|>TT9|" + str("|".join(list_bat_loi)) + "*|*" 
    loai_nha_tho_cu_log = "|>TT10|" + str(loai_nha_tho_cu) + "*|*"
    thoi_gian_su_dung_log = "|>TT11|" + str(thoi_gian_su_dung) + "*|*"
    dien_tich_san_xd_log = "|>TT12|" + str(dien_tich_san_xd) + "*|*"
    # KET QUA
    gia_truoc_log = "|>KQ1|" + str(gia_thi_truong) + "*|*" 
    gia_sau_log = "|>KQ2|" + str(gia_dieu_chinh) + "*|*" 
    don_gia_ctxd_log = "|>KQ14|" + str(don_gia_ctxd) + "*|*"
    tong_gia_ctxd_log = "|>KQ15|" + str(tong_gia_ctxd) + "*|*"
    tong_gia_tai_san_log = "|>KQ16|" + str(tong_gia_tai_san) + "*|*"

    du_lieu_nhap = dia_chi_log + vi_tri_bds_log + dien_tich_log + mat_tien_log + hinh_dang_log + do_rong_ngo_log + truc_chinh_log + loi_the_log + bat_loi_log + loai_nha_tho_cu_log + thoi_gian_su_dung_log + dien_tich_san_xd_log
    ket_qua = gia_truoc_log + gia_sau_log + don_gia_ctxd_log + tong_gia_ctxd_log + tong_gia_tai_san_log

    event_log = Event_log(new_id, session['username'], ngay_khoi_tao, 'bds_tho_cu', du_lieu_nhap, ket_qua)
    db.session.add(event_log)
    # TAO TICKET
   
    new_ticket = Id_ticket(
                new_id,
                dia_chi_cu_the,
                vi_tri_bds,
                dien_tich_dat_thi_truong,
                do_rong_mat_tien_thi_truong,
                hinh_dang,
                do_rong_ngo_thi_truong,
                kcach_truc_chinh_thi_truong,
                "|".join(list_loi_the),
                "|".join(list_bat_loi),
                gia_thi_truong,
                gia_dieu_chinh,
                loai_nha_tho_cu,
                thoi_gian_su_dung,
                don_gia_ctxd,
                dien_tich_san_xd,
                session['username'],
                ngay_khoi_tao,
            )

    db.session.add(new_ticket)
    db.session.commit()
    return jsonify({ 
                    'new_id' : new_id,
                    'ngay_khoi_tao' : ngay_khoi_tao,
                    'gia_truoc' : gia_thi_truong,
                    'gia_sau' : gia_dieu_chinh,
                    'dia_chi' : dia_chi,
                    'vi_tri' : vi_tri_bds,
                    'dac_diem_vi_tri' : dac_diem_VT,
                    'do_rong_ngo' : do_rong_ngo_thi_truong,
                    'kcach' : kcach_truc_chinh_thi_truong,
                    'mat_tien' : do_rong_mat_tien_thi_truong,
                    'hinh_dang' : hinh_dang,
                    'dien_tich' : dien_tich_dat_thi_truong,
                    'list_yeu_to' : list_yeu_to,
                    'tong_gia_dat' : tong_gia_dat,
                    'list_loi_the' : list_loi_the,
                    'list_bat_loi' : list_bat_loi,
                    'loai_nha_tho_cu' : loai_nha_tho_cu,
                    'thoi_gian_su_dung' : thoi_gian_su_dung,
                    'dien_tich_san_xd' : dien_tich_san_xd,
                    'don_gia_ctxd' : don_gia_ctxd,
                    'tong_gia_ctxd' : tong_gia_ctxd,
                    'tong_gia_tai_san' : tong_gia_tai_san,                    
                    })


#---------------- PAGE ------------------
@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


# ----------------ADMIN -----------------
@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():    
    return render_template('login.html')
    

@app.route('/authentication', methods=['GET', 'POST'])
def authentication():
    user = request.args["usn"].lower()
    pwd = request.args["pd"]
    check_user = db.session.query(User_SM.phan_quyen, User_SM.name, User_SM.trang_thai).filter_by(username = user, passhash = hash_user(pwd)).distinct().all()
    if check_user:
        if check_user[0].trang_thai == 'Active':
            session['username'] = user
            session['filter_user'] = user
            session['password'] = pwd
            session['role'] = check_user[0].phan_quyen
            session['name'] = check_user[0].name
            session['logged_in'] = True
            user = db.session.query(User_SM.name, User_SM.cmnd, User_SM.mail, User_SM.sdt, User_SM.username, User_SM.ngay_khoi_tao, User_SM.phan_quyen, User_SM.trang_thai, User_SM.ngay_doi_pass).filter_by(username = session['username']).all()[0]
            ngay_tao = dt.datetime.now()
            ngay_doi = str_to_dt(user.ngay_doi_pass.split(".")[0])
            thoi_gian_doi_pass = ngay_tao - ngay_doi
            if 'days' in str(thoi_gian_doi_pass):
                if int(str(thoi_gian_doi_pass).split(" day")[0]) > 60:
                    print(int(str(thoi_gian_doi_pass).split(" day")[0]))
                    session['changepass'] = False
                    return jsonify({'result' : 'changepass'})
                else:
                    session['changepass'] = True  
            else:
                session['changepass'] = True    
            return jsonify({'result' : 'check_gia'})
        elif check_user[0].trang_thai == 'Deactive':
            return jsonify({'result' : 'deactive'})
    else:
        return jsonify({'result' : 'login'})


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('login'))


# ----------------ADMIN -----------------
@app.route('/ad_cnb_min',methods=['GET', 'POST'])
@login_required
@changepass_required
@admin_required
def ad_cnb_min():
    return render_template(
        "trang-chu/admin.html",
        
    )


###################
@app.route('/check_gia',methods=['GET', 'POST'])
@login_required
@changepass_required
# @admin_required
def check_gia():
    list_tinh_thanh_uy_ban = [r[0] for r in db.session.query(Khung_gia_uy_ban.thanh_pho).distinct().order_by(Khung_gia_uy_ban.thanh_pho.asc()).all()]
    list_thanh_pho_quy_hoach = [r[0] for r in db.session.query(Quy_hoach.thanh_pho).distinct().order_by(Quy_hoach.thanh_pho.asc()).all()]    
    list_hinh_dang_bds = [r[0] for r in db.session.query(Hinh_dang.hinh_dang).distinct().order_by(Hinh_dang.hinh_dang.asc()).all()]
    list_loai_nha  = [r[0] for r in db.session.query(Loai_nha.loai_nha).distinct().order_by(Loai_nha.loai_nha.asc()).all()]
    list_nam_su_dung  = [r[0] for r in db.session.query(Nam_su_dung.thoi_gian).distinct().order_by(Nam_su_dung.thoi_gian.asc()).all()]
    Id_ticket = [r[0] for r in db.session.query(Event_log.id_ticket).order_by(Event_log.id_ticket.desc()).limit(3000).all()]
    return render_template(
        "tool_calculate_template/check_gia.html",
        list_hinh_dang_bds = list_hinh_dang_bds,
        list_tinh_thanh_uy_ban = list_tinh_thanh_uy_ban,
        list_thanh_pho_quy_hoach = list_thanh_pho_quy_hoach,
        list_loai_nha = list_loai_nha,
        list_nam_su_dung = list_nam_su_dung,
        Id_ticket = Id_ticket
    )


@app.route('/tin_tuc/danh_muc_tin_tuc',methods=['GET', 'POST'])
@login_required
@admin_required
def form_tin_tuc():
    return render_template(
        "trang-chu/tin_tuc/danh_muc_tin_tuc.html",        
    )


@app.route('/user/profile',methods=['GET', 'POST'])
@login_required
@changepass_required
def profile():
    user = db.session.query(User_SM.name, User_SM.cmnd, User_SM.mail, User_SM.sdt, User_SM.username, User_SM.ngay_khoi_tao, User_SM.phan_quyen, User_SM.trang_thai, User_SM.ngay_doi_pass).filter_by(username = session['username']).all()[0]
    return render_template(
        "user/profile.html", user = user,
    )


@app.route('/filter_user', methods=['GET', 'POST'])
def filter_user():
    session['filter_user'] = request.args['user'].split(" < ")[1][:-2]
    return '1'


@app.route('/user/dashboard',methods=['GET', 'POST'])
@login_required
@changepass_required
@admin_required
def dashboard():
    # session['filter_user'] = 'namht3'
    # CHART
    user = db.session.query(User_SM.name, User_SM.cmnd, User_SM.mail, User_SM.sdt, User_SM.username, User_SM.ngay_khoi_tao, User_SM.phan_quyen, User_SM.trang_thai, User_SM.ngay_doi_pass).filter_by(username = session['filter_user']).all()[0]
    all_user = db.session.query(User_SM.name, User_SM.username).distinct().order_by(User_SM.username.asc()).all()
    count_gtt_query = db.session.query(Event_log).filter_by(username = session['filter_user']).filter(or_(Event_log.phan_loai == 'bds_tho_cu', Event_log.phan_loai == 'bds_biet_thu', Event_log.phan_loai == 'bds_chung_cu', Event_log.phan_loai == 'bds_nghi_duong')).count()
    count_roa_query = db.session.query(Event_log).filter_by(username = session['filter_user']).filter(or_(Event_log.phan_loai == 'roa_bds_tho_cu', Event_log.phan_loai == 'roa_bds_biet_thu', Event_log.phan_loai == 'roa_bds_chung_cu', Event_log.phan_loai == 'roa_bds_nghi_duong')).count()
    count_ttqh_query = db.session.query(Event_log).filter_by(username = session['filter_user'], phan_loai = 'quy_hoach').count()
    count_all_query = count_gtt_query + count_roa_query + count_ttqh_query
    count_query_in_month = db.session.query(Event_log).filter_by(username = session['filter_user']).filter(or_(Event_log.time_process.like(f'%{dt.datetime.now().year}-{dt.datetime.now().month}-%'), Event_log.time_process.like(f'%{dt.datetime.now().year}-0{dt.datetime.now().month}-%'))).count()
    list_month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
    count_gtt_query_per_month = []
    count_roa_query_per_month = []
    count_ttqh_query_per_month = []
    list_month_categories = []
    for r in list_month:
        gtt_query_per_month = db.session.query(Event_log).filter_by(username = session['filter_user']).filter(or_(Event_log.phan_loai == 'bds_tho_cu', Event_log.phan_loai == 'bds_biet_thu', Event_log.phan_loai == 'bds_chung_cu', Event_log.phan_loai == 'bds_nghi_duong')).filter(Event_log.time_process.like(f'%{dt.datetime.now().year}-{r}-%')).count()
        roa_query_per_month = db.session.query(Event_log).filter_by(username = session['filter_user']).filter(or_(Event_log.phan_loai == 'roa_bds_tho_cu', Event_log.phan_loai == 'roa_bds_biet_thu', Event_log.phan_loai == 'roa_bds_chung_cu', Event_log.phan_loai == 'roa_bds_nghi_duong')).filter(Event_log.time_process.like(f'%{dt.datetime.now().year}-{r}-%')).count()
        ttqh_query_per_month = db.session.query(Event_log).filter_by(username = session['filter_user'], phan_loai = 'quy_hoach').filter(Event_log.time_process.like(f'%{dt.datetime.now().year}-{r}-%')).count()        
        if gtt_query_per_month != 0 or roa_query_per_month != 0 or ttqh_query_per_month != 0:
            count_gtt_query_per_month.append(gtt_query_per_month)
            count_roa_query_per_month.append(roa_query_per_month)
            count_ttqh_query_per_month.append(ttqh_query_per_month)
            list_month_categories.append(f'Tháng {r}')
    # STAT BOX
    all_user_query = db.session.query(User_SM.username).distinct().all()
    count_all_query_all_user = db.session.query(Event_log).count()

    count_query_in_last_month_all_user = db.session.query(Event_log).filter(or_(Event_log.time_process.like(f'%{dt.datetime.now().year}-{dt.datetime.now().month-1}-%'), Event_log.time_process.like(f'%{dt.datetime.now().year}-0{dt.datetime.now().month-1}-%'))).count()

    count_query_in_month_all_user = db.session.query(Event_log).filter(or_(Event_log.time_process.like(f'%{dt.datetime.now().year}-{dt.datetime.now().month}-%'), Event_log.time_process.like(f'%{dt.datetime.now().year}-0{dt.datetime.now().month}-%'))).count()

    count_all_user = db.session.query(User_SM).count()
    count_user_in_month = db.session.query(User_SM).filter(or_(User_SM.ngay_khoi_tao.like(f'%{dt.datetime.now().year}-{dt.datetime.now().month}-%'), User_SM.ngay_khoi_tao.like(f'%{dt.datetime.now().year}-0{dt.datetime.now().month}-%'))).count()

    count_user_in_last_month = db.session.query(User_SM).filter(or_(User_SM.ngay_khoi_tao.like(f'%{dt.datetime.now().year}-{dt.datetime.now().month-1}-%'), User_SM.ngay_khoi_tao.like(f'%{dt.datetime.now().year}-0{dt.datetime.now().month-1}-%'))).count()
    return render_template(
        "user/dashboard.html", user = user,
        count_gtt_query = count_gtt_query,
        count_roa_query = count_roa_query,
        count_ttqh_query = count_ttqh_query,
        count_all_query = count_all_query,
        count_query_in_month = count_query_in_month,
        all_user_query = all_user_query,
        count_gtt_query_per_month = json.dumps(count_gtt_query_per_month),
        count_roa_query_per_month = json.dumps(count_roa_query_per_month),
        count_ttqh_query_per_month = json.dumps(count_ttqh_query_per_month),
        list_month_categories = json.dumps(list_month_categories),
        count_all_user = count_all_user,
        count_user_in_month = count_user_in_month,
        count_user_in_last_month = count_user_in_last_month,
        all_user = all_user,
        count_query_in_last_month_all_user = count_query_in_last_month_all_user,
        count_query_in_month_all_user = count_query_in_month_all_user,
        count_all_query_all_user = count_all_query_all_user,
    )


@app.route('/user/changepass',methods=['GET', 'POST'])
@login_required
def changepass():
    user = db.session.query(User_SM.name, User_SM.cmnd, User_SM.mail, User_SM.sdt, User_SM.username, User_SM.ngay_khoi_tao, User_SM.phan_quyen, User_SM.trang_thai, User_SM.ngay_doi_pass).filter_by(username = session['username']).all()[0]    
    return render_template(
        "user/changepass.html", user = user      
    )


@app.route('/ajax_change_pass', methods=['GET', 'POST'])
def ajax_change_pass():
    new_pass = request.args["new_pass"]
    user = db.session.query(User_SM).filter_by(username = session['username']).update({User_SM.password : new_pass, User_SM.passhash : hash_user(new_pass), User_SM.ngay_doi_pass : dt.datetime.now()})
    db.session.commit()
    session['changepass'] = True
    return 'Đổi mật khẩu thành công.'


@app.route('/user/quan_ly_user', methods=['GET', 'POST'])
@login_required
@changepass_required
@admin_required
def quan_ly_user():
    list_user = db.session.query(User_SM.name, User_SM.cmnd, User_SM.mail, User_SM.sdt, User_SM.username, User_SM.password, User_SM.ngay_khoi_tao, User_SM.phan_quyen, User_SM.trang_thai).all()
    return render_template(
        "user/quan_ly_user.html", list_user = list_user        
    )


@app.route('/change_user_role', methods=['GET', 'POST'])
def change_user_role():
    user = request.args["user"]
    value = request.args["value"]
    db.session.query(User_SM).filter_by(username = user).update({User_SM.phan_quyen : value})
    db.session.commit()
    return 'Done'


@app.route('/change_user_status', methods=['GET', 'POST'])
def change_user_status():
    user = request.args["user"]
    value = request.args["value"]
    db.session.query(User_SM).filter_by(username = user).update({User_SM.trang_thai : value})
    db.session.commit()
    return 'Done'


@app.route('/ajax_del_user', methods=['GET', 'POST'])
def ajax_del_user():
    del_user = request.args["del_user"]
    remove_user = db.session.query(User_SM).filter_by(username = del_user).delete()
    db.session.commit()
    return jsonify({'result' : del_user})


@app.route('/user/query_history', methods=['GET', 'POST'])
@login_required
@changepass_required
def query_history():
    query_history_gia_thi_truong = db.session.query(Event_log.id_ticket, Event_log.time_process, Event_log.phan_loai, Event_log.du_lieu_nhap, Event_log.ket_qua).filter_by(username = session['username']).filter(or_(Event_log.phan_loai == 'bds_tho_cu', Event_log.phan_loai == 'bds_biet_thu', Event_log.phan_loai == 'bds_chung_cu', Event_log.phan_loai == 'bds_nghi_duong')).order_by(Event_log.id_ticket.desc()).all()
    query_history_roa = db.session.query(Event_log.id_ticket, Event_log.time_process, Event_log.phan_loai, Event_log.du_lieu_nhap, Event_log.ket_qua).filter_by(username = session['username']).filter(or_(Event_log.phan_loai == 'roa_bds_tho_cu', Event_log.phan_loai == 'roa_bds_biet_thu', Event_log.phan_loai == 'roa_bds_chung_cu', Event_log.phan_loai == 'roa_bds_nghi_duong')).order_by(Event_log.id_ticket.desc()).all()
    query_history_quy_hoach = db.session.query(Event_log.id_ticket, Event_log.time_process, Event_log.du_lieu_nhap, Event_log.ket_qua).filter_by(username = session['username'], phan_loai = 'quy_hoach').order_by(Event_log.id_ticket.desc()).all()
    # ID FOR SEARCH
    Id_ticket = [r[0] for r in db.session.query(Event_log.id_ticket).filter_by(username = session['username']).order_by(Event_log.id_ticket.desc()).order_by(Event_log.id_ticket.desc()).all()]
    return render_template(
        "user/query_history.html", query_history_gia_thi_truong = query_history_gia_thi_truong, Id_ticket = Id_ticket, query_history_roa = query_history_roa, query_history_quy_hoach = query_history_quy_hoach,        
    )


@app.route('/user/tao_moi_user',methods=['GET', 'POST'])
@login_required
@admin_required
def tao_moi_user():
    user_field = ['name', 'cmnd', 'mail', 'sdt', 'username', 'password', 'passhash', 'ngay_khoi_tao', 'phan_quyen', 'trang_thai']
    if request.method == 'POST':
        name = request.form.get("name")
        cmnd = request.form.get("cmnd")
        email = request.form.get("email")
        username = request.form.get("username").lower()
        password = request.form.get("password")
        role = request.form.get("role")
        status = request.form.get("status")
        sdt = request.form.get("sdt")
        new_user = User_SM(name, cmnd, email, sdt, username, password, hash_user(password), str(dt.datetime.now()), role, status, dt.datetime(2000,1,1))
        db.session.add(new_user)
        db.session.commit()
    return render_template(
        "user/tao_moi_user.html",        
    )


if __name__ == '__main__':
    # Run the app on all available interfaces on port 80 which is the
    # standard port for HTTP
    db.create_all()
    app.run()
    # app.debug = True
    # port = int(os.environ.get("PORT", 33507))
    # app.run(
    #     host="0.0.0.0",
    #     port=port,
    # )