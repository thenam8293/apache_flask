# -*- coding: utf8 -*-

import pypyodbc
import xlrd
import datetime as dt
import sqlite3

# def sql(query,var=''):
#     # connection = pypyodbc.connect('Driver={SQL Server};Server=10.62.24.161\SQLEXPRESS;Database=web_cong_viec_amc;uid=aos;pwd=aos159753')
#     # connection = pypyodbc.connect('Driver={SQL Server};Server=10.62.24.161\SQLEXPRESS;Database=AMC_B;uid=aos;pwd=aos159753')
#     # connection = pypyodbc.connect('Driver={SQL Server};Server=10.62.24.161\SQLEXPRESS;Database=web_cong_viec_amc;uid=aos;pwd=aos159753')
#     connection = pypyodbc.connect('Driver={SQL Server};Server=10.62.24.161\SQLEXPRESS;Database=WEB_CONG_VIEC;uid=aos;pwd=aos159753')
#     # connection = pypyodbc.connect('Driver={SQL Server};Server=10.62.24.123,1433\SQL2008;Database=web_cong_viec_amc_mien_nam;uid=phunq;pwd=aos159753')

#     cursor = connection.cursor()
#     cursor.execute(query,var)
#     if query.lower()[:6] == 'select':
#         x = cursor.fetchall()
#         cursor.close()
#         return x
#     else:
#         cursor.commit()
#         cursor.close()

def sqlite(query,var=''):
    connection = sqlite3.connect(r'sm_tool.db')
    cursor = connection.cursor()
    cursor.execute(query,var)
    if query.lower()[:6] == 'select':
        x = cursor.fetchall()
        connection.close()
        return x
    elif query.lower()[:6] == 'create':
        connection.close()
    else:
        connection.commit()
        connection.close()


def str_to_dt(x):
    try:
        return dt.datetime.strptime(x,'%H:%M %d/%m/%Y')
    except:
        return dt.datetime.strptime(x,'%d/%m/%Y')


############## ROA NGHI DUONG
# n = xlrd.open_workbook(r'roa_nd.xlsx')
# sheet = n.sheet_by_index(0)
# print(sheet.nrows)
# for i in range(1, sheet.nrows):
#     print(i)
#     ten_du_an = str(sheet.cell(i,1).value).strip()
#     ten_duong = str(sheet.cell(i,2).value).strip()
#     loai_nha = str(sheet.cell(i,3).value).strip()
#     ma_can = str(sheet.cell(i,4).value).strip()
#     roa1 = str(sheet.cell(i,5).value).strip()
#     tong_gt = str(sheet.cell(i,6).value).strip()
#     loi_nhuan = str(sheet.cell(i,7).value).strip()
#     dia_chi = str(sheet.cell(i,8).value).strip()
#     loai_bds = str(sheet.cell(i,9).value).strip()

#     r = [ten_du_an, ten_duong, loai_nha, ma_can, dia_chi, roa1, tong_gt, loi_nhuan, loai_bds, 1, 1, 1, 1]
#     print(r)
#     sqlite("INSERT INTO roa_nghi_duong values({})".format(",".join(["?"]*13)), r)


# ############## ROA BIET THU
# n = xlrd.open_workbook(r'roa_bt.xlsx')
# sheet = n.sheet_by_index(0)
# print(sheet.nrows)
# for i in range(1, sheet.nrows):
#     print(i)
#     stt = str(sheet.cell(i,0).value).strip()
#     ten_du_an = str(sheet.cell(i,1).value).strip()
#     ten_duong = str(sheet.cell(i,2).value).strip()
#     loai_nha = str(sheet.cell(i,3).value).strip()

#     ma_can = str(sheet.cell(i,4).value).strip()
#     if ma_can[-2:] == '.0':
#         ma_can = ma_can[:-2]
#     roa1 = str(sheet.cell(i,5).value).strip()
#     tong_gt = str(sheet.cell(i,6).value).strip()
#     loi_nhuan = str(sheet.cell(i,7).value).strip()
#     dia_chi = str(sheet.cell(i,8).value).strip()
#     loai_bds = str(sheet.cell(i,9).value).strip()


#     r = [ten_du_an, ten_duong, loai_nha, ma_can, dia_chi, roa1, tong_gt, loi_nhuan, loai_bds, 1, 1, 1, 1]
#     print(r)
#     sqlite("INSERT INTO roa_biet_thu values({})".format(",".join(["?"]*13)), r)


############## ROA CAN HO CHUNG CU
# n = xlrd.open_workbook(r'roa_cc.xlsx')
# sheet = n.sheet_by_index(0)
# for i in range(1,sheet.nrows):
#     print(i)
#     stt = str(sheet.cell(i,0).value).strip()
#     ten_du_an = str(sheet.cell(i,1).value).strip()
#     toa = str(sheet.cell(i,2).value).strip()
#     tang = str(sheet.cell(i,3).value).strip()
#     ma_can = str(sheet.cell(i,4).value).strip()
#     if ma_can[-2:] == '.0':
#         ma_can = ma_can[:-2]
#     if tang[-2:] == '.0':
#         tang = tang[:-2]   

#     # dien_tich = (str(sheet.cell(i,6).value).strip()
#     # accc = (str(sheet.cell(i,5).value).strip()
#     roa1 = str(sheet.cell(i,6).value).strip()
#     tong_gt = str(sheet.cell(i,8).value).strip() if str(sheet.cell(i,8).value).strip() != '' else 'NULL'
#     loi_nhuan = str(sheet.cell(i,9).value).strip() if str(sheet.cell(i,9).value).strip() != '' else 'NULL'
#     dia_chi = str(sheet.cell(i,10).value).strip()
#     dien_tich = str(sheet.cell(i,5).value).strip()
#     don_gia = str(sheet.cell(i,7).value).strip()
#     r = [ten_du_an, toa, tang, ma_can, dia_chi, roa1, dien_tich, tong_gt, loi_nhuan, don_gia, 1, 1, 1]
#     print(r)
# #     try:
#     sqlite("INSERT INTO roa_chung_cu values({})".format(",".join(["?"]*13)), r)
#     except:
#         print(r)
#         pass

################### ROA THO CU
# n = xlrd.open_workbook(r'roa_tc.xlsx')
# sheet = n.sheet_by_index(0)
# print(sheet.nrows)
# for i in range(1, sheet.nrows):
#     print(i)
#     stt = str(sheet.cell(i,0).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     tp = str(sheet.cell(i,1).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     quan = str(sheet.cell(i,2).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     duong = str(sheet.cell(i,3).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     doan_duong = str(sheet.cell(i,4).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     # vi_tri = 'Vị trí ' + (str(sheet.cell(i,5).value).strip()).replace('.0', '')
#     vi_tri = str(sheet.cell(i,5).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     r1 = float(str(sheet.cell(i,6).value).strip()) if str(sheet.cell(i,6).value).strip() != '' else 1
#     r2 = float(str(sheet.cell(i,7).value).strip()) if str(sheet.cell(i,7).value).strip() != ''  else 1
#     r3 = float(str(sheet.cell(i,8).value).strip()) if str(sheet.cell(i,8).value).strip() != ''  else 1
#     r4 = float(str(sheet.cell(i,9).value).strip()) if str(sheet.cell(i,9).value).strip() != ''  else 1
#     r5 = float(str(sheet.cell(i,10).value).strip()) if str(sheet.cell(i,10).value).strip() != ''  else 1
#     r6 = float(str(sheet.cell(i,11).value).strip()) if str(sheet.cell(i,11).value).strip() != ''  else 1
#     r7 = float(str(sheet.cell(i,12).value).strip()) if str(sheet.cell(i,12).value).strip() != ''  else 1
    
#     roa = str(sheet.cell(i,13).value).strip()
#     dia_chi = doan_duong + ', ' + duong + ', ' + quan + ', ' + tp
#     r = [tp, quan, duong, doan_duong, vi_tri, dia_chi, r1, r2, r3,r4, r5, r6, r7, roa]
#     print(r)
#     sqlite("INSERT INTO roa_tho_cu values({})".format(",".join(["?"]*14)), r)



################### ROA MIN THO CU
# n = xlrd.open_workbook(r'roa_min_tc.xlsx')
# sheet = n.sheet_by_index(0)
# print(sheet.nrows)
# for i in range(1, sheet.nrows):
#     print(i)
#     stt = str(sheet.cell(i,0).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     tp = str(sheet.cell(i,1).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     quan = str(sheet.cell(i,2).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     duong = str(sheet.cell(i,3).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     doan_duong = str(sheet.cell(i,4).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     # vi_tri = 'Vị trí ' + (str(sheet.cell(i,5).value).strip()).replace('.0', '')
#     vi_tri = str(sheet.cell(i,5).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
    
#     roa = str(sheet.cell(i,6).value).strip()
#     gia_tt = str(sheet.cell(i,7).value).strip()
#     r = [tp, quan, duong, doan_duong, vi_tri, roa, gia_tt]
#     print(r)
#     sqlite("INSERT INTO roa_min_tho_cu values({})".format(",".join(["?"]*7)), r)


############## ROA MIN DU AN
# n = xlrd.open_workbook(r'roa_min_du_an.xlsx')
# sheet = n.sheet_by_index(0)
# for i in range(1,sheet.nrows):
#     print(i)
#     stt = str(sheet.cell(i,0).value).strip()
#     ten_du_an = str(sheet.cell(i,1).value).strip()
#     loai_bds = str(sheet.cell(i,2).value).strip()
#     toa = str(sheet.cell(i,3).value).strip()
#     tang = str(sheet.cell(i,4).value).strip() if str(sheet.cell(i,8).value).strip() != '' else '.'
#     ma_can = str(sheet.cell(i,5).value).strip()
#     if ma_can[-2:] == '.0':
#         ma_can = ma_can[:-2]
#     if tang[-2:] == '.0':
#         tang = tang[:-2]   
#     dia_chi = str(sheet.cell(i,6).value).strip()
#     roa = str(sheet.cell(i,7).value).strip()
#     tong_gt = str(sheet.cell(i,8).value).strip() if str(sheet.cell(i,8).value).strip() != '' else 'NULL'
#     r = [ten_du_an, toa, tang, ma_can, dia_chi, roa, loai_bds, tong_gt]
#     print(r)
#     sqlite("INSERT INTO roa_min_du_an values({})".format(",".join(["?"]*8)), r)


################ BDS THO CU
# n = xlrd.open_workbook(r'tc.xlsx')
# sheet = n.sheet_by_index(0)
# print (sheet.nrows)
# for i in range(1,sheet.nrows):
#     print(i)
#     name1 = sheet.cell(i,1).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name2 = sheet.cell(i,2).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name3 = sheet.cell(i,3).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name4 = sheet.cell(i,4).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name5 = sheet.cell(i,5).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name6 = str(sheet.cell(i,7).value).strip().split(".0")[0].replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name8 = 'Chưa xác định'
#     # name9 = sheet.cell(i,9).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "")

#     name10 = str(sheet.cell(i,10).value).strip().replace("\r\n", "").replace("\r", "").replace("\n", "")
#     name9 = name4 + ', ' + name3 + ', ' + name2 + ', ' + name1
#     # name9 = i
#     r = [name1, name2, name3, name4, name5, '0', name6, name8, name9, name10]
#     print(r)
#     sqlite("INSERT INTO data_mb values({})".format(",".join(["?"]*10)), r)


###################### BDS CHUNG CU CAN HO

# n = xlrd.open_workbook(r'cc.xlsx')
# sheet = n.sheet_by_index(0)
# print (sheet.nrows)
# for i in range(1,sheet.nrows):
#     print(i)
#     name0 = sheet.cell(i,0).value
#     name1 = str(sheet.cell(i,1).value).strip()
#     try:
#         name2 = int(sheet.cell(i,2).value)
#     except:
#         name2 = str(sheet.cell(i,2).value).strip()
#     try:
#         name3 = int(sheet.cell(i,3).value)
#     except:
#         name3 = str(sheet.cell(i,3).value).strip()
    # try:
#         name4 = int(sheet.cell(i,4).value)
#     except:
#         name4 = str(sheet.cell(i,4).value).strip()
#     name4 = str(sheet.cell(i,4).value).strip()
#     name5 = str(sheet.cell(i,5).value).strip()
#     name6 = str(sheet.cell(i,6).value).strip()
#     name7 = float(str(sheet.cell(i,7).value))
#     name8 = str(sheet.cell(i,8).value).strip()
#     # name9 = i
#     r = [name1, name2, name3, name4, name5, name6, name7, name8]
#     print(r)
#     sqlite("INSERT INTO data_chung_cu values({})".format(",".join(["?"]*8)), r)


################ BIET THU LIEN KE
# n = xlrd.open_workbook(r'bt.xlsx')
# sheet = n.sheet_by_index(0)
# print (sheet.nrows)
# for i in range(1,sheet.nrows):
#     print(i)
#     name0 = str(sheet.cell(i,0).value).strip()
#     name1 = str(sheet.cell(i,1).value).strip()
#     name2 = str(sheet.cell(i,2).value).strip()
#     try:
#         name3 = int(sheet.cell(i,3).value)
#     except:
#         name3 = str(sheet.cell(i,3).value).strip()
#     try:
#         name4 = int(sheet.cell(i,4).value)
#     except:
#         name4 = str(sheet.cell(i,4).value).strip()
#     name5 = str(sheet.cell(i,5).value).strip()
#     name6 = str(sheet.cell(i,6).value).strip()
#     name7 = str(sheet.cell(i,7).value).strip()
#     name8 = str(sheet.cell(i,8).value).strip()
#     name9 = str(sheet.cell(i,9).value).strip()
#     name10 = str(sheet.cell(i,10).value).strip()
#     name11 = str(sheet.cell(i,11).value).strip()
#     # name9 = i
#     r = [name1, name2, name3, name4, name5, name6, name7, name8, name9, name10, name11]
#     print(r)
#     sqlite("INSERT INTO bds_lien_ke_bt values({})".format(",".join(["?"]*11)), r)


############## THONG TIN QUY HOACH

# n = xlrd.open_workbook(r'quy_hoach.xlsx')
# sheet = n.sheet_by_index(0)
# print (sheet.nrows)
# for i in range(1,sheet.nrows):
#     print (i)
#     name1 = sheet.cell(i,0).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,0).value.strip() != '' else 'Chưa xác định'
#     name2 = sheet.cell(i,1).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,1).value.strip() != '' else 'Chưa xác định'
#     name3 = sheet.cell(i,2).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,2).value.strip() != '' else 'Chưa xác định'
#     name4 = sheet.cell(i,3).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,3).value.strip() != '' else 'Chưa xác định'
#     name5 = sheet.cell(i,4).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,4).value.strip() != '' else 'Chưa xác định'
#     name6 = sheet.cell(i,5).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,5).value.strip() != '' else 'Chưa xác định'
#     name7 = sheet.cell(i,6).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,6).value.strip() != '' else 'Chưa xác định'
#     name8 = sheet.cell(i,7).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") if sheet.cell(i,7).value.strip() != '' else 'Chưa xác định'
#     name9 = sheet.cell(i,8).value.strip().replace("\r\n", "").replace("\r", "").replace("\n", "") 
#     r = [name1, name2, name3, name4, name5, name6, name7, name8, name9]
#     # print(r)
#     try:
#         sqlite("INSERT INTO thong_tin_quy_hoach values({})".format(",".join(["?"]*9)), r)
#     except:
#         print(r)
#         pass

def check_null(input_):
    if input_ == '' or input_ == ' ':
        input_ = 'NULL'
    if input_ != '' and input_ != 'NULL' and input_ != ' ':
        input_ = round(float(input_), 3)
    return input_

############## KHUNG GIA NAH NUOC



# n = xlrd.open_workbook(r'kgnn.xlsx')
# sheet = n.sheet_by_index(0)
# print (sheet.nrows)
# for i in range(1,sheet.nrows):
#     print (i)
#     name1 = sheet.cell(i,0).value
#     name2 = sheet.cell(i,1).value
#     name3 = sheet.cell(i,2).value
#     name4 = sheet.cell(i,3).value

#     name5 = check_null(sheet.cell(i,4).value)
#     name6 = check_null(sheet.cell(i,5).value)
#     name7 = check_null(sheet.cell(i,6).value)
#     name8 = check_null(sheet.cell(i,7).value)
#     # name9 = check_null(sheet.cell(i,8).value)
#     name9 = 'NULL'

#     r = [name1, name2, name3, name4, name5, name6, name7, name8, name9]
#     print(r)
#     sqlite("INSERT INTO khung_gia_uy_ban values({})".format(",".join(["?"]*9)), r)


# data_mb = sqlite("""select distinct Dia_chi from Data_MB where Vi_tri = 'Vị trí 4' and Mien = 'MB' """)
# for r in data_mb:
#     data = sqlite("""select * from Data_MB where Dia_chi = ? """, r)
#     for i in range(1, len(data)):
#         if int(data[i][4].split('Vị trí ')[1]) > int(data[i-1][4].split('Vị trí ')[1]) and float(data[i][6]) > float(data[i-1][6]):
#             print(data)
#             print(int(data[i][4].split('Vị trí ')[1]), int(data[i-1][4].split('Vị trí ')[1]))
#             print(float(data[i][6]), float(data[i-1][6]))
        
   

# result = sorted([r if 'A' not in r and 'B' not in r and ',' not in r  and r != '' else 0 for r in list_1], key=lambda x: int(str(x).split("A")[0].split("B")[0].split(",")[0].split("-")[0]))
# print (result)